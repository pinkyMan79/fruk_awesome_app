class UnsafeNotificationRequest {
  final int id;
  final String notificationMessage;
  final String notificationType;
  final Map<String, dynamic> additionalInfo;
  final DateTime notificationTime;
  final int profileId;
  final bool seen;

  UnsafeNotificationRequest({
    required this.id,
    required this.notificationMessage,
    required this.notificationType,
    required this.additionalInfo,
    required this.notificationTime,
    required this.profileId,
    required this.seen,
  });

  factory UnsafeNotificationRequest.fromJson(Map<String, dynamic> json) {
    return UnsafeNotificationRequest(
      id: json['id'] ?? 0,
      notificationMessage: json['notificationMessage'] ?? '',
      notificationType: json['notificationType'] ?? '',
      additionalInfo: json['additionalInfo'] ?? {},
      notificationTime: DateTime.parse(json['notificationTime'] ?? ''),
      profileId: json['profileId'] ?? 0,
      seen: json['seen'] ?? false,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'notificationMessage': notificationMessage,
      'notificationType': notificationType,
      'additionalInfo': additionalInfo,
      'notificationTime': notificationTime.toIso8601String(),
      'profileId': profileId,
      'seen': seen,
    };
  }
}


class NotificationData {
  List<NotificationItem>? content;
  Pageable? pageable;
  bool? last;
  int? totalPages;
  int? totalElements;
  int? size;
  int? number;
  Sort? sort;
  bool? first;
  int? numberOfElements;
  bool? empty;

  NotificationData({
    this.content,
    this.pageable,
    this.last,
    this.totalPages,
    this.totalElements,
    this.size,
    this.number,
    this.sort,
    this.first,
    this.numberOfElements,
    this.empty,
  });

  factory NotificationData.fromJson(Map<String, dynamic> json) {
    return NotificationData(
      content: (json['content'] as List<dynamic>)
          .map((item) => NotificationItem.fromJson(item))
          .toList(),
      pageable: Pageable.fromJson(json['pageable']),
      last: json['last'],
      totalPages: json['totalPages'],
      totalElements: json['totalElements'],
      size: json['size'],
      number: json['number'],
      sort: Sort.fromJson(json['sort']),
      first: json['first'],
      numberOfElements: json['numberOfElements'],
      empty: json['empty'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'content': content?.map((item) => item.toJson()).toList(),
      'pageable': pageable?.toJson(),
      'last': last,
      'totalPages': totalPages,
      'totalElements': totalElements,
      'size': size,
      'number': number,
      'sort': sort?.toJson(),
      'first': first,
      'numberOfElements': numberOfElements,
      'empty': empty,
    };
  }

}

class NotificationItem {
  int? id;
  String? notificationMessage;
  String? notificationType;
  AdditionalInfo? additionalInfo;
  String? notificationTime;
  int? profileId;
  bool? seen;

  NotificationItem({
    this.id,
    this.notificationMessage,
    this.notificationType,
    this.additionalInfo,
    this.notificationTime,
    this.profileId,
    this.seen,
  });

  factory NotificationItem.fromJson(Map<String, dynamic> json) {
    return NotificationItem(
      id: json['id'],
      notificationMessage: json['notificationMessage'],
      notificationType: json['notificationType'],
      additionalInfo: AdditionalInfo.fromJson(json['additionalInfo']),
      notificationTime: json['notificationTime'],
      profileId: json['profileId'],
      seen: json['seen'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'notificationMessage': notificationMessage,
      'notificationType': notificationType,
      'additionalInfo': additionalInfo?.toJson(),
      'notificationTime': notificationTime,
      'profileId': profileId,
      'seen': seen,
    };
  }

}

class AdditionalInfo {
  int? lessonId;
  String? lessonName;
  int? courseId;
  String? courseName;
  String? createdAt;

  AdditionalInfo({
    this.lessonId,
    this.lessonName,
    this.courseId,
    this.courseName,
    this.createdAt,
  });

  factory AdditionalInfo.fromJson(Map<String, dynamic> json) {
    return AdditionalInfo(
      lessonId: json['lessonId'],
      lessonName: json['lessonName'],
      courseId: json['courseId'],
      courseName: json['courseName'],
      createdAt: json['createdAt'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'lessonId': lessonId,
      'lessonName': lessonName,
      'courseId': courseId,
      'courseName': courseName,
      'createdAt': createdAt,
    };
  }

}

class Pageable {
  Sort? sort;
  int? pageNumber;
  int? pageSize;
  int? offset;
  bool? unpaged;
  bool? paged;

  Pageable({
    this.sort,
    this.pageNumber,
    this.pageSize,
    this.offset,
    this.unpaged,
    this.paged,
  });

  factory Pageable.fromJson(Map<String, dynamic> json) {
    return Pageable(
      sort: Sort.fromJson(json['sort']),
      pageNumber: json['pageNumber'],
      pageSize: json['pageSize'],
      offset: json['offset'],
      unpaged: json['unpaged'],
      paged: json['paged'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'sort': sort?.toJson(),
      'pageNumber': pageNumber,
      'pageSize': pageSize,
      'offset': offset,
      'unpaged': unpaged,
      'paged': paged,
    };
  }

}

class Sort {
  bool? sorted;
  bool? empty;
  bool? unsorted;

  Sort({
    this.sorted,
    this.empty,
    this.unsorted,
  });

  factory Sort.fromJson(Map<String, dynamic> json) {
    return Sort(
      sorted: json['sorted'],
      empty: json['empty'],
      unsorted: json['unsorted'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'sorted': sorted,
      'empty': empty,
      'unsorted': unsorted,
    };
  }

}

