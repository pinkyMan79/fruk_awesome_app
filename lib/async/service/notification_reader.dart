import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:http/http.dart' as http;
import 'package:workmanager/workmanager.dart';
import 'fruk_api.dart';

void initScheduledNotificationJob() {
  WidgetsFlutterBinding.ensureInitialized();
  Workmanager().initialize(callbackDispatcher);
  Workmanager().registerPeriodicTask("3", "notificationTask");
}

void callbackDispatcher() {
  Workmanager().executeTask((task, inputData) async {
    while (true) {
      await sendNotificationRequest(0, 999);
      await Future.delayed(const Duration(seconds: 10)); // Задержка перед следующим запросом
    }
  });
}

