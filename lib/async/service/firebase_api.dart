import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class FirebaseApi {
  var fireMessaging = FirebaseMessaging.instance;

   Future<void> initInvocation() async {
    await fireMessaging.requestPermission();
    var token = await fireMessaging.getToken();
    print('msg token $token');
    initPushNotifications();
    print('init push');
    subscribeToTopic();
    print('subscribed to topic');
  }

  void handleMessage(RemoteMessage? msg) {
    if (msg == null) return;
    print(msg.data);
    print("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
  }

  Future initPushNotifications() async {
    FirebaseMessaging.instance.getInitialMessage().then(handleMessage);
    FirebaseMessaging.onMessageOpenedApp.listen(handleMessage);
  }

  Future<void> subscribeToTopic() async {
    final FirebaseMessaging messaging = FirebaseMessaging.instance;
    const String topic = 'new_notification';
    await messaging.subscribeToTopic(topic);
  }

  Future<void> sendMessageToTopic() async {
    final FirebaseMessaging messaging = FirebaseMessaging.instance;
    const RemoteNotification notification = RemoteNotification(
      title: 'A new notification is available',
      body: 'Check out our latest app in the app store.',
    );

    final message = const RemoteMessage(
      notification: notification
    );

    await messaging.sendMessage(to: 'new_notification');
    print('Message to topic sent successfully!!');
  }


}