import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:fruk_app/screen/common.dart';
import 'package:fruk_app/screen/common.dart';
import 'package:fruk_app/async/model/NotificationModel.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:riverpod/riverpod.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'firebase_api.dart';

import '../../constants/HeaderConstants.dart';
import '../../constants/HostConstants.dart';

enum Type {
  NEW_TICKET,
  NEW_NEWS,
  LESSON_EDIT,
  LESSON_ADD,
  COURSE_ACCESS,
  NEW_REPORT,
  REPORT_APPROVE,
  LESSON_APPROVE,
  LESSON_SENT,
  RESET_COUNT
}

Future<void> resetNotifications(Type type) async {
  final String typeValue = type.name;
  final Uri url = Uri.parse('https://apidev.baze.pro/v1/notifications/reset?type=$typeValue');
  final dio = Dio();
  final Map<String, String> headers = {
    HeaderConstants.CONTENT_TYPE: HeaderConstants.CONTENT_TYPE_VALUE,
    HeaderConstants.ORIGIN: HostConstants.HOST,
    HeaderConstants.REFERER: HostConstants.HOST
  };

  try {
    final response = await dio.patch(
      url.toString(),
      options: Options(
        headers: headers
      )
    );

    if (response.statusCode == 200) {
      print('Запрос успешно выполнен');
    } else {
      print('Ошибка запроса: ${response.statusCode}');
    }
  } catch (error) {
    print('Ошибка: $error');
  }
}

Future<NotificationData?> sendNotificationRequest(int page, int limit) async {
  final Uri url = Uri.parse('https://apidev.baze.pro/v1/notifications?page=$page&limit=$limit');
  final String? token = await fetchData();
  String resolvedToken = "";
  if (token == null) {
    // re login process
  } else {
    resolvedToken = token;
  }
  final dio = Dio();

  final Map<String, String> headers = {
    HeaderConstants.CONTENT_TYPE: HeaderConstants.CONTENT_TYPE_VALUE,
    HeaderConstants.AUTHORIZATION: resolvedToken,
    HeaderConstants.ORIGIN: HostConstants.HOST,
    HeaderConstants.REFERER: HostConstants.HOST
  };

  try {
    final response = await dio.get(
      url.toString(),
      options: Options(
        headers: headers
      )
    );

    if (response.statusCode == 200) {
      print(response.data);
      var data = response.data;
      var notificationData = NotificationData.fromJson(response.data);

      var notif = await getNotification();
      if (notif == null) {
        saveNotification(notificationData);
        print('Запрос успешно выполнен в фоне и выгружен в сессию');
        print("push init");
        print("saved to session storage");
        return notificationData;
      }

      if (notif.content!.length != notificationData.content!.length) {
        // init push
        print('Запрос успешно выполнен в фоне');
        print("push init");
        saveNotification(notif);
        // send push
      }
      return Future(() => notificationData);
    } else {
      print('Ошибка запроса: ${response.statusCode}');
      return Future(() => NotificationData());

    }
  } catch (error) {
    print('Ошибка: $error');
  }
  return null;
}

Future<NotificationData?> CASAndSendNotificationRequest(int page, int limit) async {
  final Uri url = Uri.parse('https://apidev.baze.pro/v1/notifications?page=$page&limit=$limit');
  final String? token = await fetchData();
  String resolvedToken = "";
  if (token == null) {
    // re login process
  } else {
    resolvedToken = token;
  }
  final dio = Dio();

  final Map<String, String> headers = {
    HeaderConstants.CONTENT_TYPE: HeaderConstants.CONTENT_TYPE_VALUE,
    HeaderConstants.AUTHORIZATION: resolvedToken,
    HeaderConstants.ORIGIN: HostConstants.HOST,
    HeaderConstants.REFERER: HostConstants.HOST
  };

  try {
    final response = await dio.get(
        url.toString(),
        options: Options(
            headers: headers
        )
    );

    if (response.statusCode == 200) {
      print(response.data);
      var data = response.data;
      var notificationData = NotificationData.fromJson(response.data);
      saveNotification(notificationData);
      FirebaseApi().sendMessageToTopic();
      return Future(() => notificationData);
    } else {
      print('Ошибка запроса: ${response.statusCode}');
      return Future(() => NotificationData());
    }
  } catch (error) {
    print('Ошибка: $error');
  }
  return null;
}