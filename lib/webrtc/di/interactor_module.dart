
import '../domain/interactors/webrtc_interactor.dart';
import 'injector.dart';

void initInteractorModule() {
  i.registerFactory<WebrtcInteractor>(() => WebrtcInteractor(i.get()));
}