import 'package:fruk_app/webrtc/di/repository_module.dart';
import 'package:get_it/get_it.dart';

import 'cubit_module.dart';
import 'data_source_module.dart';
import 'interactor_module.dart';

GetIt get i => GetIt.instance;

void initInjector() {
  initDataSourceModule();
  initRepositoryModule();
  initInteractorModule();
  initCubitModule();
}