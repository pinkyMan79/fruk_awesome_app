
import '../presentation/pages/webrtc/webrtc_cubit.dart';
import 'injector.dart';

void initCubitModule() {
  i.registerFactory<WebrtcCubit>(() => WebrtcCubit(i.get()));
}