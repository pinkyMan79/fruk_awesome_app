
import '../data/datasource.dart';
import 'injector.dart';

void initDataSourceModule() {
  i.registerSingleton<RemoteDataSource>(
    RemoteDataSource(),
  );
}