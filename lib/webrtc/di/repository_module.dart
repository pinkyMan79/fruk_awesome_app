import '../domain/repositories/auth_repository.dart';
import '../domain/repositories/room_repository.dart';
import '../repository/auth_repository.dart';
import '../repository/room_repository.dart';
import 'injector.dart';

void initRepositoryModule() {
  i.registerSingleton<RoomRepositoryInt>(RoomRepository(i.get()) as RoomRepositoryInt);
  i.registerSingleton<AuthRepositoryInt>(AuthRepository(i.get()) as AuthRepositoryInt);
}