class HeaderConstants {
  static const String AUTHORIZATION = "Authorization";
  static const String CONTENT_TYPE = "Content-Type";
  static const String ORIGIN = "Origin";
  static const String REFERER = "Referer";
  static const String CONTENT_TYPE_VALUE = "application/json; charset=utf-8";

}