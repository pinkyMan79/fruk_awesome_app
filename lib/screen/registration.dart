import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:fruk_app/model/user/UserRegisterRequest.dart';
import 'package:flutter/material.dart';
import 'package:fruk_app/screen/login.dart';

import '../constants/HeaderConstants.dart';
import '../constants/HostConstants.dart';

class RegistrationScreen extends StatelessWidget {
  final TextEditingController nameController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();
  final TextEditingController emailController = TextEditingController();

  Future<void> register(BuildContext context) async {
    String name = nameController.text.trim();
    String phone = phoneController.text.trim();
    String email = emailController.text.trim();

    // Создание объекта для отправки данных на сервер
    RegistrationData registrationData = RegistrationData(name: name, phone: phone, email: email);

    final Uri url = Uri.parse('https://apidev.baze.pro/v1/user/request-register');

    final dio = Dio();

    final Map<String, String> headers = {
      HeaderConstants.CONTENT_TYPE: HeaderConstants.CONTENT_TYPE_VALUE,
      HeaderConstants.ORIGIN: HostConstants.HOST,
      HeaderConstants.REFERER: HostConstants.HOST
    };


    try {
      final response = await dio.post(
        url.toString(),
        options: Options(
          headers: headers
        ),
        data: jsonEncode(registrationData.toJson()),
      );

      if (response.statusCode == 200) {
        var body = response.data; // here is your response JSON model
        print('Registration successful: $body');
        // Добавьте здесь дополнительную логику после успешной регистрации\
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => LoginScreen()), // Замените ВашНовыйЭкран на фактический экран, на который вы хотите перейти
        );
      } else {
        // Обработка ошибки
        print('Error during registration: ${response.statusCode}');
      }
    } catch (error) {
      print('Error: $error');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Registration'),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            TextField(
              controller: nameController,
              decoration: InputDecoration(
                labelText: 'Name',
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(height: 12.0),
            TextField(
              controller: phoneController,
              decoration: InputDecoration(
                labelText: 'Phone',
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(height: 12.0),
            TextField(
              controller: emailController,
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                labelText: 'Email',
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(height: 20.0),
            ElevatedButton(
                onPressed: () async {
                  // Simulating an asynchronous login action
                  await register(context);
                },
                child: Text('Register'),
            ),
          ],
        ),
      ),
    );
  }
}
