import 'package:flutter/material.dart';
import 'common.dart';
import 'package:fruk_app/async/model/NotificationModel.dart';
import 'package:fruk_app/async/service/fruk_api.dart';
import 'menu.dart';

class NotificationScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<NotificationData?>(
      future: getNotification(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(child: CircularProgressIndicator()); // Показать индикатор загрузки во время загрузки данных
        } else if (snapshot.hasError || !snapshot.hasData || snapshot.data!.content == null) {
          return Center(
            child: Text('No notifications available'), // Показать сообщение, если нет данных
          );
        } else {
          final notificationData = snapshot.data!;
          return Scaffold(
            appBar: AppBar(
              title: Text('Notification Items'),
            ),
            drawer: MenuScreen(),
            body: ListView.builder(
              itemCount: notificationData.content!.length,
              itemBuilder: (context, index) {
                final item = notificationData.content![index];
                final color = item.seen ?? false ? const Color(0xAEF6D1FF) : const Color(0xFF5252FF);
                return Container(
                  margin: EdgeInsets.symmetric(vertical: 4.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 1,
                        blurRadius: 3,
                        offset: Offset(0, 2), // changes position of shadow
                      ),
                    ],
                  ),
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    elevation: 0, // Set to 0 to disable the card's default elevation
                    color: color,
                    child: ListTile(
                      title: Text(item.notificationMessage ?? ''),
                      subtitle: Text(item.notificationTime ?? ''),
                      // Другие данные для отображения по вашему выбору
                    ),
                  ),
                );
              },
            ),
          );
        }
      },
    );
  }
}
