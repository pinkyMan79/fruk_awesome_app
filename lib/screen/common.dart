import 'dart:collection';

import 'package:fruk_app/async/service/fruk_api.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fruk_app/async/model/NotificationModel.dart';
import 'package:flutter/material.dart';
import 'package:riverpod/riverpod.dart';
import 'dart:convert';
import '../constants/HeaderConstants.dart';
import '../constants/HostConstants.dart';
import 'menu.dart';
import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<String?> fetchData() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  String? savedToken = prefs.getString('token');
  String? savedProfileId = prefs.getString('profile_id');
  if (savedToken != null && savedProfileId != null) {
    print('Token: $savedToken');
    print('Profile ID: $savedProfileId');
    return savedToken;
  } else {
    print('Данные не найдены в SharedPreferences');
    return null;
  }
}

const Color darkPurple = Color(0xFF8761CE);
const Color whitePurple = Color(0xFFE0D2FC);

Future<void> logoutUser() async {
  // Сначала делаем запрос на logout пользователя
  try {
    var fetchData_ = await fetchData();
    final Map<String, String> headers = {
      HeaderConstants.CONTENT_TYPE: HeaderConstants.CONTENT_TYPE_VALUE,
      HeaderConstants.ORIGIN: HostConstants.HOST,
      HeaderConstants.REFERER: HostConstants.HOST,
      'accept': '*/*',
      'Authorization': fetchData_ ?? ''
    };

    var logoutResponse = await Dio().post(
      'https://apidev.baze.pro/v1/logout',
      options: Options(
        headers: headers
      ),
    );

    if (logoutResponse.statusCode == 200) {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      if (prefs.getString('token') != null ) {
        // clear local storage
        prefs.setString('token', '');
      }
      prefs.clear();

    } else {
      print('Ошибка при разлогинивании пользователя');
    }
  } catch (e) {
    print('Произошла ошибка при logout: $e');
  }
}

Set<NotificationData> notificationSet = HashSet();
final notificationProvider = StateProvider<Set<NotificationData>>((ref) => {});

// Сериализация объекта в формат JSON
String notificationToJson(NotificationData? notification) {
  if (notification == null) return '';
  return json.encode(notification.toJson());
}

// Десериализация строки JSON обратно в объект
NotificationData? jsonToNotification(String json) {
  if (json.isEmpty) return null;
  return NotificationData.fromJson(jsonDecode(json));
}

// Сохранение в SharedPreferences
Future<void> saveNotification(NotificationData? notification) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String notificationJson = notificationToJson(notification);
  await prefs.setString('notification', notificationJson);
}

// Получение из SharedPreferences
Future<NotificationData?> getNotification() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String? notificationJson = prefs.getString('notification');
  var jsonToNotif = jsonToNotification(notificationJson ?? '');
  // if null -> send request to be side
  jsonToNotif ??= await CASAndSendNotificationRequest(0, 999);
  return jsonToNotif;
}

//Получение ProfileType из SharedPreferences
Future<String?> getProfileType() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString('profile_type');
}