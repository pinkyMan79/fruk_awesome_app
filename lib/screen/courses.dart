import 'package:dio/dio.dart';
import 'package:fruk_app/constants/HeaderConstants.dart';
import 'package:fruk_app/constants/HostConstants.dart';
import 'package:fruk_app/constants/security/ProfileTypeConstants.dart';
import 'package:fruk_app/model/course/response/PageCourseListResponse.dart';
import 'package:flutter/material.dart';
import 'block.dart';
import 'common.dart';

import '../model/course/response/CourseListResponse.dart';
import 'create_course_page.dart';
import 'edit_course.dart';
import 'menu.dart';
import 'package:flutter_gen/gen_l10n/app_localization.dart';


class CourseListPage extends StatelessWidget {

  static const int MAX_COURSE_NAME_LENGTH = 50;
  static const int MAX_COURSE_DESCRIPTION_LENGTH = 30;
  const CourseListPage({super.key});

  Future<Widget> fetchCourses(int limit, int page, BuildContext contex, bool haveControlCompany) async {
    final Uri url =
        Uri.parse('https://apidev.baze.pro/v1/course?limit=$limit&page=$page');

    PageCourseListResponse? pageCourseListResponse;
    final dio = Dio();
    var s = await fetchData();
    final Map<String, String> headers = {
      HeaderConstants.CONTENT_TYPE: HeaderConstants.CONTENT_TYPE_VALUE,
      HeaderConstants.AUTHORIZATION: s.toString(),
      HeaderConstants.ORIGIN: HostConstants.HOST,
      HeaderConstants.REFERER: HostConstants.HOST
    };
    try {
      final response =
          await dio.get(url.toString(), options: Options(headers: headers));
      if (response.statusCode == 200) {
        var body = response.data; // here is our json model
        pageCourseListResponse = PageCourseListResponse.fromJson(body);
        print("DONEEEEEE");
        print(pageCourseListResponse.content);
      } else {
        // Обработка ошибки
        print('Ошибка входа: ${response.statusCode}');
      }
    } catch (error) {
      print('Ошибка: $error');
    }

    return Scaffold(
      body: ListView.builder(
        itemCount: pageCourseListResponse?.content.length ?? 0,
        // Use null-aware operator and provide a default value
        itemBuilder: (context, index) {
          CourseListResponse? course = pageCourseListResponse?.content[index];

          return buildCourseCard(course!, headers, context, haveControlCompany);
        },
      ),
      drawer: MenuScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    var haveControlCompany = getProfileType() == ProfileTypeConstants.CONTROL_COMPANY;
    return Scaffold(
        appBar: AppBar(
          title: Text(AppLocalizations.of(context)!.list_of_courses),
        ),
      drawer: MenuScreen(),
      body: FutureBuilder(
        future: fetchCourses(999, 0, context, haveControlCompany),
        builder: (BuildContext context, AsyncSnapshot<Widget> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.waiting:
              // Возвращайте заглушку или индикатор загрузки
              return CircularProgressIndicator();
            case ConnectionState.done:
              // Возвращайте виджет, построенный на основе полученных данных
              if (snapshot.hasError) {
                return Text(AppLocalizations.of(context)!.error + ': ${snapshot.error}');
              }
              return snapshot.data ?? Text(AppLocalizations.of(context)!.no_data);
              default:
              return Text(AppLocalizations.of(context)!.invalid_state + ': ${snapshot.connectionState}');
            }
        },
      ),
      floatingActionButton: Visibility(
        visible: haveControlCompany, // Устанавливаем видимость в зависимости от привилегий
        child: FloatingActionButton(
          onPressed: () {
            // Navigate to the page for creating a new course
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => CreateCoursePage(),
              ),
            );
          },
          child: Icon(Icons.add),
          backgroundColor: darkPurple,
        ),
      ),
    );
  }

    Widget buildCourseCard(
      CourseListResponse course, Map<String, String> headers, BuildContext context, bool isControlCompany) {
    String truncatedName = course.name.length > MAX_COURSE_NAME_LENGTH
        ? '${course.name.substring(0, MAX_COURSE_NAME_LENGTH)}...'
        : course.name;

    String truncatedDescription = course.description.length >
        MAX_COURSE_DESCRIPTION_LENGTH
        ? '${course.description.substring(0, MAX_COURSE_DESCRIPTION_LENGTH)}...'
        : course.description;
    return Card(
      elevation: 3,
      margin: EdgeInsets.all(8),
      child: Stack(
        alignment: AlignmentDirectional.topEnd,
        // Выравнивание в правом верхнем углу
        children: [
          ListTile(
            contentPadding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            // Измененные отступы
            leading: CircleAvatar(
              backgroundColor: Colors.transparent,
              child: Container(
                width: 60,
                height: 60,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(
                      'https://apidev.baze.pro${course.attachmentsUrls
                          .urls['cover']}',
                      headers: headers,
                    ),
                  ),
                ),
              ),
            ),
            title: Text(
              truncatedName,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => BlocksScreen(
                    courseId: course.id,
                  ),
                ),
              );
            },
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  truncatedDescription,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
                SizedBox(height: 8),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('${course.countStudents} ' + AppLocalizations.of(context)!.students + ' ${course
                        .countLessons} ' + AppLocalizations.of(context)!.lessons),
                    Icon(
                      course.passed ? Icons.check_circle : Icons.cancel,
                      color: course.passed ? Colors.green : Colors.red,
                    ),
                  ],
                ),
              ],
            ),
            // onTap: () {
            //   // Handle the tap
            // },
          ),
          Visibility(
            visible: isControlCompany,
            child: Positioned(
              top: 8, // Отступ от верхнего края
              right: 8, // Отступ от правого края
              child: IconButton(
                icon: Icon(Icons.edit, size: 20),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => EditCoursePage(courseId: course.id),
                    ),
                  );
                },
              ),
            ),
          )
        ],
      ),
    );
  }
}
