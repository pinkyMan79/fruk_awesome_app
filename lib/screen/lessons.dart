import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

import '../model/course/response/CourseListResponse.dart';
import 'block.dart';
import 'common.dart';

class LessonPage extends StatefulWidget {
  final int lessonId;
  final int courseId;

  LessonPage({required this.lessonId, required this.courseId});

  @override
  _LessonPageState createState() => _LessonPageState();
}

class _LessonPageState extends State<LessonPage> {
  int? lessonId;
  int? courseId;
  String? courseName;
  String? lessonsContent;

  Future<CourseListResponse> fetchCourseData(int courseId) async {
    final String str =
        'https://apidev.baze.pro/v1/course/$courseId'; // Replace with your API endpoint

    final Uri url = Uri.parse(str);

    final dio = Dio();
    var s = await fetchData();
    final Map<String, dynamic> headers = {
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization': s,
      'accept': '*/*',
      'Origin': 'https://demo.baze.pro',
      'Referer': 'https://demo.baze.pro/'
    };

    try {
      final response =
      await dio.get(url.toString(), options: Options(headers: headers));
      if (response.statusCode == 200) {
        var body = response.data;
        CourseListResponse page = CourseListResponse.fromJson(body);
        return page;
      } else {
        // Обработка ошибки
        print('Ошибка входа: ${response.statusCode}');
        throw Exception('Failed to load lesson responses');
      }
    } catch (error) {
      print('Ошибка: $error');
      throw Exception('Failed to load lesson responses');
    }
  }

  Future<void> _fetchLessonResponses() async {
    final String str =
        'https://apidev.baze.pro/v1/lesson/$lessonId/template'; // Replace with your API endpoint

    final Uri url = Uri.parse(str);

    final dio = Dio();
    var s = await fetchData();
    final Map<String, dynamic> headers = {
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization': s,
      'accept': '*/*',
      'Origin': 'https://demo.baze.pro',
      'Referer': 'https://demo.baze.pro/'
    };

    try {
      final response =
          await dio.get(url.toString(), options: Options(headers: headers));
      if (response.statusCode == 200) {
        String body = response.data.toString();
        print('BODDDDY');
        print(body);

        String cleanedString = body.replaceAll(RegExp(r'^\[\[\{|\}\]\]$'), '');

        List<String> splitData = cleanedString.split(', data: ');
        print(splitData[1]);

        cleanedString = splitData[1];

        splitData = cleanedString.split(', buttonName:');
        cleanedString = splitData[0];
        lessonsContent = cleanedString;
        setState(() {
          lessonsContent = cleanedString;
        });
        // return page;
      } else {
        // Обработка ошибки
        print('Ошибка входа: ${response.statusCode}');
        throw Exception('Failed to load lesson responses');
      }
    } catch (error) {
      print('Ошибка: $error');
      throw Exception('Failed to load lesson responses');
    }
  }

  @override
  void initState() {
    super.initState();
    lessonId = widget.lessonId;
    _fetchLessonResponses();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: FutureBuilder<CourseListResponse>(
          future: fetchCourseData(widget.courseId),
          builder: (BuildContext context, AsyncSnapshot<CourseListResponse> snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Text('Loading...'); // Display loading text while fetching data
            } else if (snapshot.hasError) {
              return Text('Error'); // Display error if fetching fails
            } else {
              // Use the fetched data to update the AppBar title
              return Text(snapshot.data!.name); // Assuming 'courseTitle' is the property you want to display
            }
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 20),
              lessonsContent != null
                  ? Html(data: lessonsContent)
                  : Center(child: CircularProgressIndicator()),
            ],
          ),
        ),
      ),
    );
  }
}