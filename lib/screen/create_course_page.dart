import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:http_parser/http_parser.dart';
import 'package:image_picker/image_picker.dart';

import '../constants/HeaderConstants.dart';
import '../constants/HostConstants.dart';
import 'common.dart';

class CreateCoursePage extends StatefulWidget {
  static const String CREATE_COURSE_ENDPOINT =
      "https://apidev.baze.pro/v1/company/course";

  @override
  _CreateCoursePageState createState() => _CreateCoursePageState();
}

class _CreateCoursePageState extends State<CreateCoursePage> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  File? _coverImageFile;
  File? _avatarImageFile;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Создать курс'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              TextFormField(
                controller: _nameController,
                decoration: InputDecoration(
                  labelText: 'Название курса',
                  border: OutlineInputBorder(),
                ),
              ),
              SizedBox(height: 16),
              TextFormField(
                controller: _descriptionController,
                decoration: InputDecoration(
                  labelText: 'Описание курса',
                  border: OutlineInputBorder(),
                ),
                maxLines: 3,
                textAlignVertical: TextAlignVertical.center,
              ),
              SizedBox(height: 16),
              Wrap(
                spacing: 16.0,
                runSpacing: 16.0,
                children: [
                  _coverImageFile != null
                      ? Container(
                    height: 100,
                    width: 100,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      image: DecorationImage(
                        image: FileImage(_coverImageFile!),
                        fit: BoxFit.cover,
                      ),
                    ),
                  )
                      : SizedBox.shrink(),
                  ElevatedButton(
                    onPressed: () => _pickImage(ImageType.cover),
                    child: Text('Выбрать изображение cover'),
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8),
                      ),
                    ),
                  ),
                  _avatarImageFile != null
                      ? Container(
                    height: 100,
                    width: 100,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      image: DecorationImage(
                        image: FileImage(_avatarImageFile!),
                        fit: BoxFit.cover,
                      ),
                    ),
                  )
                      : SizedBox.shrink(),
                  ElevatedButton(
                    onPressed: () => _pickImage(ImageType.avatar),
                    child: Text('Выбрать изображение avatar'),
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 16),
              ElevatedButton(
                onPressed: () async {
                  String name = _nameController.text;
                  String description = _descriptionController.text;

                  FormData formData = FormData.fromMap({
                    'name': name,
                    'description': description,
                    if (_coverImageFile != null)
                      "cover": await MultipartFile.fromFile(_coverImageFile!.path, contentType: MediaType("image", "jpeg")),
                    if (_avatarImageFile != null)
                      "avatar": await MultipartFile.fromFile(_avatarImageFile!.path, contentType: MediaType("image", "jpeg")
                      ),
                  });
                  final dio = Dio();
                  var s = await fetchData();
                  final Map<String, String> headers = {
                    HeaderConstants.CONTENT_TYPE: "multipart/form-data",
                    HeaderConstants.AUTHORIZATION: s.toString(),
                    HeaderConstants.ORIGIN: HostConstants.HOST,
                    HeaderConstants.REFERER: HostConstants.HOST
                  };
                  try {
                    var response = await dio.post(
                      CreateCoursePage.CREATE_COURSE_ENDPOINT,
                      options: Options(
                        headers: headers,
                      ),
                      data: formData,
                    );

                    print(response);

                    if (response.statusCode == 200) {
                      showFeedbackSnackBar('Курс успешно создан');
                    } else {
                      showFeedbackSnackBar(
                          'Не удалось создать курс. Пожалуйста, попробуйте еще раз.');
                    }
                  } catch (error) {
                    if (error.toString().contains("403")) {
                      showFeedbackSnackBar('Недостаточно прав');
                    } else if (error.toString().contains("400")) {
                      print(error);
                      showFeedbackSnackBar('Некорректные данные ${error}');
                    } else {
                      showFeedbackSnackBar('Произошла ошибка');
                    }
                  }

                  print('Course Name: $name');
                  print('Course Description: $description');
                },
                child: Text('Создать курс'),
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _pickImage(ImageType imageType) async {
    final ImagePicker _picker = ImagePicker();
    final XFile? image =
    await _picker.pickImage(source: ImageSource.gallery);

    if (image != null) {
      setState(() {
        if (imageType == ImageType.cover) {
          _coverImageFile = File(image.path);
        } else {
          _avatarImageFile = File(image.path);
        }
      });
    }
  }

  void showFeedbackSnackBar(String message) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(message),
        duration: Duration(seconds: 2),
      ),
    );
  }
}

enum ImageType {
  cover,
  avatar,
}
