import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import '../constants/HeaderConstants.dart';
import '../constants/HostConstants.dart';
import 'common.dart';
import 'package:flutter_gen/gen_l10n/app_localization.dart';


class InitPasswordResetScreen extends StatelessWidget {
  final TextEditingController emailController = TextEditingController();

  Future<void> _initResetPassword(BuildContext context) async {
    String email = emailController.text.trim();

    final Uri url = Uri.parse('https://apidev.baze.pro/v1/user/init-password-reset');
    final dio = Dio();

    final Map<String, String> headers = {
      HeaderConstants.CONTENT_TYPE: HeaderConstants.CONTENT_TYPE_VALUE,
      HeaderConstants.ORIGIN: HostConstants.HOST,
      HeaderConstants.REFERER: HostConstants.HOST
    };


    try {
      final response = await dio.post(
        url.toString(),
        options: Options(
          headers: headers
        ),
        data: jsonEncode({'email': email}),
      );

      if (response.statusCode == 200) {
        // Перенаправление пользователя на другой экран после успешной инициализации сброса пароля
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => ConfirmPasswordResetScreen()),
        );
      } else {
        // Обработка ошибки
        print(AppLocalizations.of(context)!.password_reset_initialization_error  + ': ${response.statusCode}');
      }
    } catch (error) {
      print(AppLocalizations.of(context)!.error + ': $error');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.reset_password),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            TextField(
              controller: emailController,
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                labelText: AppLocalizations.of(context)!.email,
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(height: 20.0),
            ElevatedButton(
              onPressed: () => _initResetPassword(context),
              child: Text(AppLocalizations.of(context)!.initiate_password_reset),
            ),
          ],
        ),
      ),
    );
  }
}

class ConfirmPasswordResetScreen extends StatelessWidget {
  final TextEditingController tokenController = TextEditingController();
  final TextEditingController newPasswordController = TextEditingController();
  final TextEditingController confirmPasswordController = TextEditingController();

  Future<void> _resetPassword() async {
    String newPassword = newPasswordController.text.trim();
    String confirmPassword = confirmPasswordController.text.trim();
    String? token = await fetchData();

    final Uri url = Uri.parse('https://apidev.baze.pro/v1/user/reset-password');

    if (token == null) {
      print("Not found token");
      throw Exception("Rejected. Reason: token not found");
    }

    final dio = Dio();

    final Map<String, dynamic> headers = {
      'Content-Type': 'application/json; charset=utf-8',
      'accept': '*/*',
      'Origin': 'https://demo.baze.pro',
      'Referer': 'https://demo.baze.pro/'
    };

    try {
      final response = await dio.patch(
        url.toString(),
        options: Options(
          headers: headers
        ),
        data: jsonEncode({
          'token': token,
          'password': newPassword,
          'passwordConfirmation': confirmPassword,
        }),
      );

      if (response.statusCode == 200) {
        // Обработка успешного сброса пароля
        print('Пароль успешно изменен!');
      } else {
        // Обработка ошибки
        print('Ошибка сброса пароля: ${response.statusCode}');
      }
    } catch (error) {
      print('Ошибка : $error');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.confirm_password_reset),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            TextField(
              controller: tokenController,
              decoration: InputDecoration(
                labelText: AppLocalizations.of(context)!.token,
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(height: 12.0),
            TextField(
              controller: newPasswordController,
              obscureText: true,
              decoration: InputDecoration(
                labelText: AppLocalizations.of(context)!.new_password,
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(height: 12.0),
            TextField(
              controller: confirmPasswordController,
              obscureText: true,
              decoration: InputDecoration(
                labelText: AppLocalizations.of(context)!.confirm_new_password,
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(height: 20.0),
            ElevatedButton(
              onPressed: _resetPassword,
              child: Text(AppLocalizations.of(context)!.reset_password),
            ),
          ],
        ),
      ),
    );
  }
}

