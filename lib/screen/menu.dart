import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fruk_app/screen/courses.dart';
import 'package:fruk_app/screen/login.dart';
import 'package:fruk_app/screen/registration.dart';
import 'package:fruk_app/screen/reccomendation.dart';
import 'notification.dart';
import 'management.dart';
import 'common.dart';
import 'package:flutter_gen/gen_l10n/app_localization.dart';

class MenuScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
           DrawerHeader(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/images/newLogo.png'),
                    // Замените на путь к вашей картинке
                    fit: BoxFit.cover,
                    scale: 0.5),
                color: darkPurple),
            child: Text(AppLocalizations.of(context)!.menu,
              style: TextStyle(color: Colors.white, fontSize: 25),
            ),
          ),
          ListTile(
            leading: Icon(Icons.verified_user),
            title: Text(AppLocalizations.of(context)!.management),
            onTap: () => {
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (context) => UserListScreen()),
              )
            },
          ),
          ListTile(
            leading: Icon(Icons.settings),
            title: Text(AppLocalizations.of(context)!.courses),
            onTap: () => {
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (context) => CourseListPage()),
              )
            },
          ),
          ListTile(
            leading: Icon(Icons.border_color),
            title: Text(AppLocalizations.of(context)!.tickets),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text(AppLocalizations.of(context)!.reports),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text(AppLocalizations.of(context)!.notifications),
            onTap: () => {
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (context) => NotificationScreen()),
              )
            },
          ),
          ListTile(
            leading: const Icon(Icons.exit_to_app),
            title: const Text('Совет'),
            onTap: () => {Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => ReccomendationPage()),
            )},
          ),
          ListTile(
            leading: const Icon(Icons.exit_to_app),
            title: Text(AppLocalizations.of(context)!.exit),
            onTap: () async {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Text(AppLocalizations.of(context)!.exit),
                    content: Text(AppLocalizations.of(context)!.are_u_sure),
                    actions: <Widget>[
                      TextButton(
                        onPressed: () {
                          Navigator.of(context).pop(); // Закрыть диалоговое окно
                        },
                        child: Text(AppLocalizations.of(context)!.cancel),
                      ),
                      TextButton(
                        onPressed: () async {
                          Navigator.of(context).pop(); // Закрыть диалоговое окно
                          await logoutUser(); // Дождаться завершения процесса logoutUser
                          // Закрыть приложение
                          await SystemChannels.platform.invokeMethod('SystemNavigator.pop');
                        },
                        child: Text(AppLocalizations.of(context)!.exit),
                      ),
                    ],
                  );
                },
              );
            },
          ),
        ],
      ),
    );
  }
}
