import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:fruk_app/model/test/response/TestQuestionResponse.dart';
import 'package:fruk_app/screen/quiz.dart';
import 'package:fruk_app/constants/HostConstants.dart';
import 'package:fruk_app/constants/HeaderConstants.dart';

import '../constants/HeaderConstants.dart';
import '../constants/HostConstants.dart';
import '../model/course/response/CourseListResponse.dart';
import '../model/test/response/TestQuestionResponse.dart';
import 'common.dart';

class LessonPage extends StatefulWidget {

  static final String GET_LESSON_TASKS = "https://apidev.baze.pro/v1/lesson/";

  final int lessonId;
  final int courseId;

  LessonPage({required this.lessonId, required this.courseId});

  @override
  _LessonPageState createState() => _LessonPageState();
}

class _LessonPageState extends State<LessonPage> {
  int? lessonId;
  String? lessonsContent;

  Future<CourseListResponse> fetchCourseData(int courseId) async {
    final String str =
        'https://apidev.baze.pro/v1/course/$courseId'; // Replace with your API endpoint

    final Uri url = Uri.parse(str);

    final dio = Dio();
    var s = await fetchData();
    final Map<String, dynamic> headers = {
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization': s,
      'accept': '*/*',
      'Origin': 'https://demo.baze.pro',
      'Referer': 'https://demo.baze.pro/'
    };

    try {
      final response =
      await dio.get(url.toString(), options: Options(headers: headers));
      if (response.statusCode == 200) {
        var body = response.data;
        CourseListResponse page = CourseListResponse.fromJson(body);
        return page;
      } else {
        // Обработка ошибки
        print('Ошибка входа: ${response.statusCode}');
        throw Exception('Failed to load lesson responses');
      }
    } catch (error) {
      print('Ошибка: $error');
      throw Exception('Failed to load lesson responses');
    }
  }

  Future<void> _fetchLessonResponses() async {
    final String str =
        'https://apidev.baze.pro/v1/lesson/$lessonId/template'; // Replace with your API endpoint

    final Uri url = Uri.parse(str);

    final dio = Dio();
    var s = await fetchData();
    final Map<String, dynamic> headers = {
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization': s,
      'accept': '*/*',
      'Origin': 'https://demo.baze.pro',
      'Referer': 'https://demo.baze.pro/'
    };

    try {
      final response =
          await dio.get(url.toString(), options: Options(headers: headers));
      if (response.statusCode == 200) {
        String body = response.data.toString();

        String cleanedString = body.replaceAll(RegExp(r'^\[\[\{|\}\]\]$'), '');

        List<String> splitData = cleanedString.split(', data: ');
        print(splitData[1]);
        cleanedString = splitData[1];
        lessonsContent = cleanedString;
        setState(() {
          lessonsContent = cleanedString;
        });
        // return page;
      } else {
        // Обработка ошибки
        print('Ошибка входа: ${response.statusCode}');
        throw Exception('Failed to load lesson responses');
      }
    } catch (error) {
      print('Ошибка: $error');
      throw Exception('Failed to load lesson responses');
    }
  }

  @override
  void initState() {
    super.initState();
    lessonId = widget.lessonId;
    _fetchLessonResponses();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: FutureBuilder<CourseListResponse>(
          future: fetchCourseData(widget.courseId),
          builder: (BuildContext context, AsyncSnapshot<CourseListResponse> snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Text('Loading...'); // Display loading text while fetching data
            } else if (snapshot.hasError) {
              return Text('Error'); // Display error if fetching fails
            } else {
              // Use the fetched data to update the AppBar title
              return Text(snapshot.data!.name); // Assuming 'courseTitle' is the property you want to display
            }
          },
        ),
      ),
      body: Column(
        children: [
          Flexible(
            child: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 20),
                    lessonsContent != null
                        ? Html(data: lessonsContent)
                        : Center(child: CircularProgressIndicator()),
                  ],
                ),
              ),
            ),
            flex: 1,
          ),
        ],
      ),
      floatingActionButton: Align(
        alignment: Alignment.bottomRight,
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: ElevatedButton(
            onPressed: () async {
              final dio = Dio();
              var s = await fetchData();
              final Map<String, String> headers = {
                HeaderConstants.CONTENT_TYPE: HeaderConstants.CONTENT_TYPE_VALUE,
                HeaderConstants.AUTHORIZATION: s.toString(),
                HeaderConstants.ORIGIN: HostConstants.HOST,
                HeaderConstants.REFERER: HostConstants.HOST
              };
              var response = await dio.get(LessonPage.GET_LESSON_TASKS + '${lessonId}' + '/tests',
                  options: Options(
                      headers: headers
                  )
              );
              var data = response.data;
              print(data);
              List<TestQuestionResponse> questions = (data as List<dynamic>)
                  .map((test) => TestQuestionResponse.fromJson(test)).toList();
              showQuizPage(context, questions, lessonId!);
            },
            child: Text('Пройти тест'),
          ),
        ),
      ),
    );
  }
  void showQuizPage(BuildContext context, List<TestQuestionResponse> questions, int lessonId) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => QuizPage(questions: questions, lessonId: lessonId),
      ),
    );
  }
}
