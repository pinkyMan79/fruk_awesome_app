import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fruk_app/model/course/response/CourseListResponse.dart';
import 'package:fruk_app/model/course/response/ReccomendedCourseResponse.dart';
import 'package:lottie/lottie.dart';

import '../constants/HeaderConstants.dart';
import '../constants/HostConstants.dart';
import '../model/course/response/PageCourseListResponse.dart';
import '../model/course/short/CourseShortRequest.dart';
import 'common.dart';
import 'menu.dart';

class ReccomendationPage extends StatefulWidget {
  @override
  _ReccomendationPageState createState() => _ReccomendationPageState();
}

class _ReccomendationPageState extends State<ReccomendationPage> {
  List<String> list = [];
  bool isLoading = true;
  bool showButton = true;
  ReccommendedCoursesResponse? resp;
  String courseName = '';
  String report = '';

  Future<List<CourseListResponse>> fetchCoursesData() async {
    final String baseUrl = 'https://apidev.baze.pro/v1/course?limit=999&page=0';

    final Uri url = Uri.parse(baseUrl);

    PageCourseListResponse? pageCourseListResponse;
    final dio = Dio();
    var s = await fetchData();
    final Map<String, String> headers = {
      HeaderConstants.CONTENT_TYPE: HeaderConstants.CONTENT_TYPE_VALUE,
      HeaderConstants.AUTHORIZATION: s.toString(),
      HeaderConstants.ORIGIN: HostConstants.HOST,
      HeaderConstants.REFERER: HostConstants.HOST
    };

    try {
      final response =
          await dio.get(url.toString(), options: Options(headers: headers));
      if (response.statusCode == 200) {
        var body = response.data;
        pageCourseListResponse = PageCourseListResponse.fromJson(body);
        return pageCourseListResponse.content;
      } else {
        throw new Exception();
      }
    } catch (error) {
      throw new Exception();
    }
  }

  Future<void> fetchDataAndSendRequest() async {
    setState(() {
      isLoading = false;
      showButton = false;
    });
    Future.delayed(Duration(seconds: 30), () {
      setState(() {
        isLoading = false;
      });
    });

    final List<CourseListResponse> courses = await fetchCoursesData();

    List<Map<String, dynamic>> transformedData = courses.map((item) {
      return {
        "id": item.id,
        "name": item.name != null ? item.name : "",
        "description": item.description != null ? item.description : "",
        "passed": item.passed ?? false,
      };
    }).toList();

    bool allFalse =
        transformedData.every((element) => element['passed'] == false);

    if (allFalse) {
      Map<String, dynamic> selectedElement = transformedData.first;

      report = selectedElement['name'];

      list.add(report);
      setState(() {
        isLoading = true;
      });
    } else {
      Map<String, dynamic> allCourses = {
        "all_courses": transformedData,
      };

      final String secondEndpoint =
          'http://192.168.205.1:5000/recommend_courses'; // Замените на свой эндпоинт;

      final Uri url = Uri.parse(secondEndpoint);
      Dio dio = Dio();

      Response response = await dio.post(
        url.toString(),
        data: jsonEncode(allCourses),
        options: Options(headers: {
          'Content-Type': 'application/json; charset=UTF-8',
          'Accept': '*/*'
        }),
      );
      if (response.statusCode == 200) {
        setState(() {
          var body = response.data;
          resp = ReccommendedCoursesResponse.fromJson(body);
          isLoading = true;
          list = resp!.recommendedCourses;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MenuScreen(),
      appBar: AppBar(
        title: Text('Рекомендованный курс'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            showButton
                ? ElevatedButton(
                    onPressed: () {
                      fetchDataAndSendRequest();
                    },
                    child: Text('Отправить запрос'),
                  )
                : Container(),
            SizedBox(height: 20),
            if (!isLoading)
              Center(
                child: Lottie.asset("assets/animation/dog_animation.json",
                    animate: true),
              )
            else
              Expanded(
                child: ListView.builder(
                  itemCount: list.length,
                  // Assuming data is a list
                  itemBuilder: (context, index) {
                    return Center(
                        child: ListTile(
                      title: Text(list[index],  textAlign: TextAlign.center,),
                    ));
                  },
                ),
              ),
          ],
        ),
      ),
    );
  }
}
