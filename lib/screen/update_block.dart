import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:http_parser/http_parser.dart';
import 'package:image_picker/image_picker.dart';

import '../constants/HeaderConstants.dart';
import '../constants/HostConstants.dart';
import 'common.dart';
import 'package:flutter_gen/gen_l10n/app_localization.dart';


class EditBlockPage extends StatefulWidget {
  final int blockId;

  static const String UPDATE_BLOCK_ENDPOINT =
      "https://apidev.baze.pro/v1/company/block";

  EditBlockPage({required this.blockId});

  @override
  _EditBlockPageState createState() => _EditBlockPageState();
}

class _EditBlockPageState extends State<EditBlockPage> {
  final TextEditingController _nameController = TextEditingController();
  File? _avatarImageFile;
  bool _isArchived = false; // Добавлено новое поле

  @override
  void initState() {
    super.initState();
    loadCourseData(widget.blockId);
  }

  void loadCourseData(int courseId) async {
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.edit_the_module),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              TextFormField(
                controller: _nameController,
                decoration: InputDecoration(
                  labelText: AppLocalizations.of(context)!.module_name,
                  border: OutlineInputBorder(),
                ),
              ),
              SizedBox(height: 16),
              Wrap(
                spacing: 16.0,
                runSpacing: 16.0,
                children: [
                  _avatarImageFile != null
                      ? Container(
                          height: 100,
                          width: 100,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            image: DecorationImage(
                              image: FileImage(_avatarImageFile!),
                              fit: BoxFit.cover,
                            ),
                          ),
                        )
                      : SizedBox.shrink(),
                  ElevatedButton(
                    onPressed: () => _pickImage(),
                    child: Text(AppLocalizations.of(context)!.select_an_image)
                  ),
                ],
              ),
              SizedBox(height: 16),
              CheckboxListTile(
                title: Text(AppLocalizations.of(context)!.archive),
                value: _isArchived,
                onChanged: (value) {
                  setState(() {
                    _isArchived = value!;
                  });
                },
              ),
              SizedBox(height: 16),
              ElevatedButton(
                onPressed: () async {
                  String name = _nameController.text;


                  FormData formData = FormData.fromMap({
                    'name': name,
                    'archive': _isArchived.toString(),
                    if (_avatarImageFile != null)
                      'avatar':
                          await MultipartFile.fromFile(_avatarImageFile!.path, contentType: MediaType("image", "jpeg")),
                  });

                  final dio = Dio();
                  var s = await fetchData();
                  final Map<String, String> headers = {
                    HeaderConstants.CONTENT_TYPE: "multipart/form-data",
                    HeaderConstants.AUTHORIZATION: s.toString(),
                    HeaderConstants.ORIGIN: HostConstants.HOST,
                    HeaderConstants.REFERER: HostConstants.HOST
                  };
                  try {
                    var response = await dio.patch(
                      EditBlockPage.UPDATE_BLOCK_ENDPOINT +
                          "${widget.blockId}",
                      options: Options(
                        headers: headers,
                      ),
                      data: formData,
                    );

                    print(response);

                    if (response.statusCode == 200) {
                      showFeedbackSnackBar(AppLocalizations.of(context)!.the_module_has_been_successfully_updated);
                    } else {
                      showFeedbackSnackBar(
                          AppLocalizations.of(context)!.the_module_could_not_be_updated_please_try_again);
                    }
                  } catch (error) {
                    if (error.toString().contains("403")) {
                      showFeedbackSnackBar(AppLocalizations.of(context)!.not_enough_rights);
                    } else if (error.toString().contains("400")) {
                      showFeedbackSnackBar(AppLocalizations.of(context)!.incorrect_data);
                    } else {
                      showFeedbackSnackBar(AppLocalizations.of(context)!.an_error_has_occurred);
                    }
                  }

                  Navigator.pop(
                      context); // Закрываем страницу редактирования после сохранения
                },
                child: Text(AppLocalizations.of(context)!.save),
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _pickImage() async {
    final ImagePicker _picker = ImagePicker();
    final XFile? image = await _picker.pickImage(source: ImageSource.gallery);

    if (image != null) {
      setState(() {
        _avatarImageFile = File(image.path);
      });
    }
  }

  void showFeedbackSnackBar(String message) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(message),
        duration: Duration(seconds: 2),
      ),
    );
  }
}
