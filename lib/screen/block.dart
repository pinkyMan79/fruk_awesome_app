import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fruk_app/model/block/BlockResponse.dart';
import 'package:fruk_app/model/block/ListBlockResponse.dart';
import 'package:fruk_app/screen/create_course_page.dart';
import 'package:fruk_app/screen/update_block.dart';

import '../constants/security/ProfileTypeConstants.dart';
import '../model/course/response/CourseListResponse.dart';
import '../model/lesson/LessonResponse.dart';
import '../model/lesson/PageLessonResponse.dart';
import 'common.dart';
import 'createLesson.dart';
import 'create_block.dart';
import 'lesson.dart';

class BlocksScreen extends StatefulWidget {
  final int courseId;
  Map<int, List<LessonResponse>> lessonMap = {};

  BlocksScreen({required this.courseId});

  @override
  _BlocksScreenState createState() => _BlocksScreenState();
}

class _BlocksScreenState extends State<BlocksScreen> {
  late Map<int, List<LessonResponse>> lessonMap = {};
  bool _isLoading = false;
  int? expandedIndex;
  late Future<List<BlockResponse>> _blockResponses;

  @override
  void initState() {
    super.initState();
    _blockResponses = fetchBlockResponses(widget.courseId);
  }

  Future<List<LessonResponse>> _fetchLessonResponses(
      int courseId, int blockId) async {
    final String str =
        'https://apidev.baze.pro/v1/lesson?courseId=$courseId&blockId=$blockId&limit=999&page=0'; // Replace with your API endpoint

    final Uri url = Uri.parse(str);

    final dio = Dio();
    var s = await fetchData();
    final Map<String, dynamic> headers = {
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization': s,
      'accept': '*/*',
      'Origin': 'https://demo.baze.pro',
      'Referer': 'https://demo.baze.pro/'
    };

    try {
      final response =
          await dio.get(url.toString(), options: Options(headers: headers));
      if (response.statusCode == 200) {
        var body = response.data;

        PageLessonResponse page = PageLessonResponse.fromJson(body);
        _isLoading = false;
        return page.content;
      } else {
        // Обработка ошибки
        print('Ошибка входа: ${response.statusCode}');
        throw Exception('Failed to load lesson responses');
      }
    } catch (error) {
      print('Ошибка: $error');
      throw Exception('Failed to load lesson responses');
    }
  }

  Future<List<BlockResponse>> fetchBlockResponses(int courseId) async {
    final String str =
        'https://apidev.baze.pro/v1/block?courseId=$courseId'; // Replace with your API endpoint

    final Uri url = Uri.parse(str);

    final dio = Dio();
    var s = await fetchData();
    final Map<String, dynamic> headers = {
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization': s,
      'accept': '*/*',
      'Origin': 'https://demo.baze.pro',
      'Referer': 'https://demo.baze.pro/'
    };

    try {
      final response =
          await dio.get(url.toString(), options: Options(headers: headers));
      if (response.statusCode == 200) {
        var body = response.data;
        ListBlockResponse page = ListBlockResponse.fromJson(body);
        return page.blocks;
      } else {
        // Обработка ошибки
        print('Ошибка входа: ${response.statusCode}');
        throw Exception('Failed to load lesson responses');
      }
    } catch (error) {
      print('Ошибка: $error');
      throw Exception('Failed to load lesson responses');
    }
  }

  Future<CourseListResponse> fetchCourseData(int courseId) async {
    final String str =
        'https://apidev.baze.pro/v1/course/$courseId'; // Replace with your API endpoint

    final Uri url = Uri.parse(str);

    final dio = Dio();
    var s = await fetchData();
    final Map<String, dynamic> headers = {
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization': s,
      'accept': '*/*',
      'Origin': 'https://demo.baze.pro',
      'Referer': 'https://demo.baze.pro/'
    };

    try {
      final response =
          await dio.get(url.toString(), options: Options(headers: headers));
      if (response.statusCode == 200) {
        var body = response.data;
        CourseListResponse page = CourseListResponse.fromJson(body);
        return page;
      } else {
        // Обработка ошибки
        print('Ошибка входа: ${response.statusCode}');
        throw Exception('Failed to load lesson responses');
      }
    } catch (error) {
      print('Ошибка: $error');
      throw Exception('Failed to load lesson responses');
    }
  }

  BlockResponse? selectedLesson;
  int? selectedIndex;

  List<LessonResponse> lessons = [];

  @override
  Widget build(BuildContext context) {
    var haveControlCompany = getProfileType() == ProfileTypeConstants.CONTROL_COMPANY;
    return Scaffold(
      appBar: AppBar(
        title: FutureBuilder<CourseListResponse>(
          future: fetchCourseData(widget.courseId),
          builder: (BuildContext context,
              AsyncSnapshot<CourseListResponse> snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Text(
                  'Loading...'); // Display loading text while fetching data
            } else if (snapshot.hasError) {
              return Text('Error'); // Display error if fetching fails
            } else {
              // Use the fetched data to update the AppBar title
              return Text(snapshot.data!
                  .name); // Assuming 'courseTitle' is the property you want to display
            }
          },
        ),
      ),
      body: FutureBuilder<List<BlockResponse>>(
        future: _blockResponses,
        builder: (BuildContext context,
            AsyncSnapshot<List<BlockResponse>> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          } else if (snapshot.hasError) {
            return Center(child: Text('Error: ${snapshot.error}'));
          } else {
            return ListView.builder(
              itemCount: snapshot.data!.length,
              itemBuilder: (context, index) {
                final block = snapshot.data![index];
                return Container(
                  margin: EdgeInsets.symmetric(vertical: 8.0),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(14.0),
                      // color: Colors.purple // Slightly darker background
                      color: whitePurple.withOpacity(0.5)),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(14.0),
                    child: ExpansionTile(
                      backgroundColor: whitePurple.withOpacity(0.5),
                      title: Text(
                        'Модуль ${index + 1}. ${block.name.toString()}',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.black),
                      ),
                      subtitle: Text('Уроки ${block.countLessons}'),
                      trailing: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Visibility(
                            visible: haveControlCompany,
                            child: IconButton(
                            icon: Icon(Icons.edit, size: 20),
                              onPressed: () {
                                Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          EditBlockPage(blockId: block.id)),
                                );
                              // Handle button press for block editing
                              },
                            ),
                          ),
                          Visibility(
                            visible: haveControlCompany,
                            child:IconButton(
                              icon: Icon(Icons.add_circle_outline, size: 20),
                              onPressed: () {
                                // Действие при нажатии на кнопку для создания урока
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => LessonCreatePage(),
                                  ),
                                );
                              },
                            )
                          ),
                        ],
                      ),
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            // borderRadius: BorderRadius.circular(12.0),
                            borderRadius: BorderRadius.circular(14.0),
                            color: whitePurple.withOpacity(0.5),
                            // Inner background color
                          ),
                          child: FutureBuilder<List<LessonResponse>>(
                            future: _fetchLessonResponses(
                                widget.courseId, block.id),
                            builder: (BuildContext context,
                                AsyncSnapshot<List<LessonResponse>>
                                    lessonSnapshot) {
                              if (lessonSnapshot.connectionState ==
                                  ConnectionState.waiting) {
                                return Center(
                                    child: CircularProgressIndicator());
                              } else if (lessonSnapshot.hasError) {
                                return Center(
                                    child:
                                        Text('Error: ${lessonSnapshot.error}'));
                              } else {
                                if (!lessonMap.containsKey(block.id)) {
                                  lessonMap[block.id] = lessonSnapshot.data!;
                                }
                                return Column(
                                  children: lessonSnapshot.data!.isEmpty
                                      ? [Text('Нет уроков для этого модуля')]
                                      : lessonSnapshot.data!.asMap().entries.map((entry) {
                                    int lessonNumber = entry.key + 1; // Получаем номер урока
                                    LessonResponse lesson = entry.value; // Получаем урок
                                    return InkWell(
                                      onTap: () {
                                        // Переход на другую страницу при выборе урока
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) => LessonPage(
                                              lessonId: lesson.id,
                                              courseId: widget.courseId,
                                            ),
                                          ),
                                        );
                                      },
                                      child: ListTile(
                                        title: Text('Урок $lessonNumber: ${lesson.name}'), // Добавляем номер урока к названию
                                      ),
                                    );
                                  }).toList(),
                                );
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
            );
          }
        },
      ),
      floatingActionButton: Visibility(
        visible: haveControlCompany,
        child: FloatingActionButton(
          onPressed: () {
            // Navigate to the page for creating a new course
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => CreateBlockPage(
                    courseId: widget
                        .courseId), // Replace with the actual page for creating a new course
              ),
            );
          },
          child: Icon(Icons.add),
          backgroundColor: Color(0xAEF6D1FF), // Adjust the color as needed
        ),
      )
    );
  }
}
