import 'dart:collection';
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fruk_app/model/test/request/CompleteAnswer.dart';
import 'package:fruk_app/screen/common.dart';

import '../constants/HeaderConstants.dart';
import '../constants/HostConstants.dart';
import '../model/test/request/CompleteLessonRequest.dart';
import '../model/test/response/LessonAnswerResponse.dart';
import '../model/test/response/TestQuestionResponse.dart';

class QuizPage extends StatefulWidget {

  static final String SEND_TO_CHECK_ANSWERS = "https://apidev.baze.pro/v1/lesson/";

  final List<TestQuestionResponse> questions;

  int lessonId;

  QuizPage({required this.questions,
  required this.lessonId});

  @override
  _QuizPageState createState() => _QuizPageState();
}

class _QuizPageState extends State<QuizPage> {
  late Map<int, CompleteAnswer> completeAnswers;

  @override
  void initState() {
    super.initState();
    completeAnswers = HashMap();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Тест'),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 20),
            Text(
              'Вопросы',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 20),
            Expanded(
              child: ListView.builder(
                itemCount: widget.questions.length,
                itemBuilder: (context, index) {
                  return buildQuestionCard(context, widget.questions[index]);
                },
              ),
            ),
            SizedBox(height: 16),
            ElevatedButton(
              onPressed: () async {
                print(completeAnswers);

                final dio = Dio();
                final s = await fetchData();
                final Map<String, String> headers = {
                  HeaderConstants.CONTENT_TYPE: HeaderConstants.CONTENT_TYPE_VALUE,
                  HeaderConstants.AUTHORIZATION: s.toString(),
                  HeaderConstants.ORIGIN: HostConstants.HOST,
                  HeaderConstants.REFERER: HostConstants.HOST
                };
                List<CompleteAnswer> comletes = [];

                completeAnswers.forEach((questionId, answers) {
                  comletes.add(answers);
                });
                CompleteLessonRequest completeLessonRequest = CompleteLessonRequest(version: 5, answers: comletes);
                print(completeLessonRequest);
                try {
                    var response = await dio.get(QuizPage.
                        SEND_TO_CHECK_ANSWERS + '${widget.lessonId}/send',
                      options: Options(
                          headers: headers
                      ),
                      data: jsonEncode(completeLessonRequest.toJson())
                    );
                    if (response.statusCode == 200) {
                      print("Тест сдан");
                    }
                } catch (error) {
                  if (error.toString().contains("403")) {
                    showFeedbackSnackBar('Недостаточно прав');
                  } else if (error.toString().contains("400")) {
                    print(error);
                    showFeedbackSnackBar('Некорректные данные');
                  } else {
                    showFeedbackSnackBar('Произошла ошибка');
                  }
                }

              },
              child: Text('Завершить тест'),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildQuestionCard(BuildContext context, TestQuestionResponse question) {
    return Card(
      elevation: 3,
      margin: EdgeInsets.symmetric(vertical: 8.0),
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              question.question ?? '',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 10),
            if (question.open == true) // Если вопрос открытый, отобразите TextField
              TextField(
                onChanged: (text) {
                  setState(() {
                    completeAnswers[question.id!] =
                        CompleteAnswer(questionId: question.id, pickedAnswer: null, answer: text, error: false, open: true);
                  });
                },
                decoration: InputDecoration(
                  hintText: 'Введите ваш ответ...',
                ),
              )
            else // Иначе отобразите варианты ответов
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: question.answers!
                    .map((answer) => buildAnswerRow(context, answer!, question.id!))
                    .toList(),
              ),
          ],
        ),
      ),
    );
  }

  Widget buildAnswerRow(BuildContext context, LessonAnswerResponse answer, int questionId) {
    return ListTile(
      title: Text(answer.text ?? ''),
      onTap: () {
        setState(() {
          completeAnswers[questionId] =
              CompleteAnswer(questionId: questionId, pickedAnswer: answer.id, answer: null, error: false, open: false);
        });
      },
    );
  }
  void showFeedbackSnackBar(String message) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(message),
        duration: Duration(seconds: 2),
      ),
    );
  }
}
