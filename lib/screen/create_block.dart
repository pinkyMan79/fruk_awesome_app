import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:http_parser/http_parser.dart';
import 'package:image_picker/image_picker.dart';

import '../constants/HeaderConstants.dart';
import '../constants/HostConstants.dart';
import 'common.dart';

class CreateBlockPage extends StatefulWidget {
  final int courseId;
  static const String CREATE_BLOCK_ENDPOINT =
      "https://apidev.baze.pro/v1/company/block";

  CreateBlockPage({required this.courseId});

  @override
  _CreateBlockPageState createState() => _CreateBlockPageState();
}

class _CreateBlockPageState extends State<CreateBlockPage> {
  late int courseId;
  final TextEditingController _nameController = TextEditingController();
  File? _avatarImageFile;

  @override
  void initState() {
    super.initState();
    courseId = widget.courseId;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Создать модуль'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              SizedBox(height: 16),
              TextFormField(
                controller: _nameController,
                decoration: InputDecoration(
                  labelText: 'Название',
                  border: OutlineInputBorder(),
                ),
                maxLines: 1,
                textAlignVertical: TextAlignVertical.center,
              ),
              SizedBox(height: 16),
              Wrap(
                spacing: 16.0,
                runSpacing: 16.0,
                children: [
                  _avatarImageFile != null
                      ? Container(
                    // width: double.infinity,
                    height: 100,
                    width: 100,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      image: DecorationImage(
                        image: FileImage(_avatarImageFile!),
                        fit: BoxFit.cover,
                      ),
                    ),
                  )
                      : SizedBox.shrink(),
                  Container(
                    width: double.infinity,

                  child:
                  ElevatedButton(

                    onPressed: () => _pickImage(),
                    child: Text('Выбрать изображение'),
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8),
                      ),
                    ),
                  ),)
                ],
              ),
              SizedBox(height: 16),
              Container(
                width: double.infinity,
                child: ElevatedButton(
                  onPressed: () async {
                    String name = _nameController.text;

                    FormData formData = FormData.fromMap({
                      'courseId': courseId,
                      'name': name,
                      if (_avatarImageFile != null)
                        'avatar':
                        await MultipartFile.fromFile(_avatarImageFile!.path, contentType: MediaType("image", "jpeg")),
                    });

                    final dio = Dio();
                    var s = await fetchData();
                    final Map<String, String> headers = {
                      HeaderConstants.CONTENT_TYPE:
                      "multipart/form-data",
                      HeaderConstants.AUTHORIZATION: s.toString(),
                      HeaderConstants.ORIGIN: HostConstants.HOST,
                      HeaderConstants.REFERER: HostConstants.HOST
                    };

                    try {
                      var response = await dio.post(
                        CreateBlockPage.CREATE_BLOCK_ENDPOINT,
                        options: Options(
                          headers: headers,
                        ),
                        data: formData,
                      );

                      print(response);

                      if (response.statusCode == 200) {
                        showFeedbackSnackBar('Модуль успешно создан');
                      } else {
                        showFeedbackSnackBar(
                            'Не удалось создать модуль. Пожалуйста, попробуйте еще раз.');
                      }
                    } catch (error) {
                      if (error.toString().contains("403")) {
                        showFeedbackSnackBar('Недостаточно прав');
                      } else if (error.toString().contains("400")) {
                        showFeedbackSnackBar('Некорректные данные');
                      } else {
                        showFeedbackSnackBar('Произошла ошибка');
                      }
                    }

                    print('Course Name: $name');
                  },
                  child: Text('Создать модуль'),
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _pickImage() async {
    final ImagePicker _picker = ImagePicker();
    final XFile? image = await _picker.pickImage(source: ImageSource.gallery);

    if (image != null) {
      setState(() {
        _avatarImageFile = File(image.path);
      });
    }
  }

  void showFeedbackSnackBar(String message) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(message),
        duration: Duration(seconds: 2),
      ),
    );
  }
}
