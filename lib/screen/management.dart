import 'dart:ffi';

import 'package:multi_select_flutter/multi_select_flutter.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fruk_app/constants/HeaderConstants.dart';
import 'package:fruk_app/constants/HostConstants.dart';
import '../constants/security/ProfileTypeConstants.dart';
import 'common.dart';
import 'menu.dart';

void main() {
  runApp(MyApp());
}

class User {
  final int? id;
  final String? email;
  final String? firstName;
  final String? lastName;
  final String? phone;
  final bool? active;
  final String? inviteRegisterDate;
  final int? banned;
  final String? profileType;
  final bool? needReports;
  final List<Role>? roles;
  final int? groupId;
  final String? franchiseName;

  User({
    this.id,
    this.email,
    this.firstName,
    this.lastName,
    this.phone,
    this.active,
    this.inviteRegisterDate,
    this.banned,
    this.profileType,
    this.needReports,
    this.roles,
    this.groupId,
    this.franchiseName,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      id: json['id'],
      email: json['email'],
      firstName: json['firstName'],
      lastName: json['lastName'],
      phone: json['phone'],
      active: json['active'],
      inviteRegisterDate: json['inviteRegisterDate'],
      banned: json['banned'],
      profileType: json['profileType'],
      needReports: json['needReports'],
      roles: (json['roles'] as List<dynamic>)
          .map((roleJson) => Role.fromJson(roleJson))
          .toList(),
      groupId: json['groupId'],
      franchiseName: json['franchiseName'],
    );
  }
}

class Role {
  final int? id;
  final String? name;

  Role({this.id, this.name});

  factory Role.fromJson(Map<String, dynamic> json) {
    return Role(id: json['id'], name: json['name']);
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: UserListScreen(),
    );
  }
}

class UserListScreen extends StatelessWidget {

  Future<List<User>> fetchUsers() async {
    const String apiUrl = 'https://apidev.baze.pro/v1/company/list-members';
    final Map<String, dynamic> requestData = {
      "profileType": null,
      "active": null,
      "banned": null
    };
    var fetchData_ = await fetchData();
    final Map<String, String> headers = {
      HeaderConstants.CONTENT_TYPE: HeaderConstants.CONTENT_TYPE_VALUE,
      HeaderConstants.ORIGIN: HostConstants.HOST,
      HeaderConstants.REFERER: HostConstants.HOST,
      'accept': '*/*',
      'Authorization': fetchData_ ?? ''
    };

    try {
      final Response response = await Dio().post(
        apiUrl,
        data: requestData,
        options: Options(
          headers: headers,
        ),
      );

      if (response.statusCode == 200) {
        final List<dynamic> responseData = response.data;
        return responseData.map((userData) => User.fromJson(userData)).toList();
      } else {
        throw Exception('Failed to load users');
      }
    } catch (e) {
      print('Error: $e');
      throw Exception('Failed to load users');
    }
  }

  @override
  Widget build(BuildContext context) {
    var haveControlCompany = getProfileType() == ProfileTypeConstants.CONTROL_COMPANY;
    return Scaffold(
      appBar: AppBar(
        title: Text('User List'),
      ),
      drawer: MenuScreen(),
      body: FutureBuilder<List<User>>(
        future: fetchUsers(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          } else if (snapshot.hasError) {
            return Center(child: Text('Error: ${snapshot.error}'));
          } else if (snapshot.hasData) {
            return SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: SingleChildScrollView(
                child: DataTable(
                  columns: const <DataColumn>[
                    DataColumn(label: Text('ID')),
                    DataColumn(label: Text('Почта')),
                    DataColumn(label: Text('Имя')),
                    DataColumn(label: Text('Фамилия')),
                    DataColumn(label: Text('Телефон')),
                    DataColumn(label: Text('Активен')),
                    DataColumn(label: Text('Заблокирован')),
                  ],
                  rows: snapshot.data!
                      .map(
                        (user) =>
                        DataRow(cells: <DataCell>[
                          DataCell(Text(user.id.toString())),
                          DataCell(Text(user.email ?? '')),
                          DataCell(Text(user.firstName ?? '')),
                          DataCell(Text(user.lastName ?? '')),
                          DataCell(Text(user.phone ?? '')),
                          DataCell(Text(user.active.toString())),
                          DataCell(Text(user.banned.toString())),
                        ]),
                  )
                      .toList(),
                ),
              ),
            );
          } else {
            return Center(child: Text('Нет доступной информации'));
          }
        },
      ),
      floatingActionButton: Visibility(
        visible: haveControlCompany,
        child: FloatingActionButton(
            onPressed: () async {
              await _showInviteUserDialog(context);
            },
            child: Icon(Icons.add),
          ),
      )
    );
  }
}

  Future<void> inviteUser(String email, String profileType, List<int> roleIds) async {
  const String apiUrl = 'https://apidev.baze.pro/v1/profile/invite';
  final Map<String, dynamic> data = {
    "email": email,
    "profileType": profileType,
    "franchiseId": null,
    "franchiseName": "",
    "groupId": null,
    "roleIds": roleIds,
  };


  try {
    var fetchData_ = await fetchData();
    final Map<String, String> headers = {
      HeaderConstants.CONTENT_TYPE: HeaderConstants.CONTENT_TYPE_VALUE,
      HeaderConstants.ORIGIN: HostConstants.HOST,
      HeaderConstants.REFERER: HostConstants.HOST,
      'accept': '*/*',
      'Authorization': fetchData_ ?? ''
    };
    final Response response = await Dio().post(
      apiUrl,
      data: data,
      options: Options(
        headers: headers,
      ),
    );

    if (response.statusCode == 200) {
      // Обновление списка пользователей после успешного приглашения
      await fetchData();
    } else {
      throw Exception('Failed to invite user');
    }
  } catch (e) {
    print('Error: $e');
    throw Exception('Failed to invite user');
  }
}

Future<List<Role>?> fetchRoles() async {
  var dio = Dio();

  try {
    var fetchData_ = await fetchData();
    final Map<String, String> headers = {
      HeaderConstants.CONTENT_TYPE: HeaderConstants.CONTENT_TYPE_VALUE,
      HeaderConstants.ORIGIN: HostConstants.HOST,
      HeaderConstants.REFERER: HostConstants.HOST,
      'accept': '*/*',
      'Authorization': fetchData_ ?? ''
    };
    var response = await dio.get(
      'https://apidev.baze.pro/v1/role?limit=999&page=0',
      options: Options(headers: headers),
    );

    if (response.statusCode == 200) {
      // Если запрос успешен (статус код 200
      var info = response.data['content'] as List<dynamic>;
      List<Role> roles = [];

      for (var roleInfo in info) {
        var role = Role(
          id: roleInfo['id'] as int,
          name: roleInfo['name'] as String,
        );
        roles.add(role);
      }
      return Future(() => roles);
      // Далее можно обрабатывать полученные данные
    } else {
      // Если запрос не успешен, например, код ошибки не 200
      print('Ошибка запроса: ${response.statusCode}');
      return Future(() => null);
    }
  } catch (e) {
    // Обработка ошибок, если они возникли при выполнении запроса
    print('Произошла ошибка: $e');
    return Future(() => null);
  }
}

Future<void> _showInviteUserDialog(BuildContext context) async {
  String email = '';
  String profileType = '';
  List<int> selectedRoleIds = [];

  await showDialog(
    context: context,
    builder: (BuildContext context) {
      return FutureBuilder<List<Role>?>(
        future: fetchRoles(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          } else if (snapshot.hasError) {
            return Center(child: Text('Error: ${snapshot.error}'));
          } else if (snapshot.hasData) {
            return AlertDialog(
              title: const Text('Добавление Пользователя'),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  TextField(
                    onChanged: (value) {
                      email = value;
                    },
                    decoration: const InputDecoration(hintText: 'Электронная Почта'),
                  ),
                  TextField(
                    onChanged: (value) {
                      profileType = value;
                    },
                    autofillHints: List.of(['STAFF', 'CONTROL_COMPANY']),
                    decoration: InputDecoration(hintText: 'Тип профиля'),
                  ),
                  MultiSelectDialogField(
                    items: snapshot.data!
                        .map((role) => MultiSelectItem<Role>(role, role.name ?? ''))
                        .toList(),
                    listType: MultiSelectListType.CHIP,
                    onConfirm: (values) {
                      selectedRoleIds = (values as List<Role>).map((role) => role.id!).toList();
                    },
                    chipDisplay: MultiSelectChipDisplay(
                      onTap: (value) {
                        selectedRoleIds.remove(value); // Удаляет выбранную роль из списка выбранных ролей
                      },
                    ),
                  ),
                ],
              ),
              actions: <Widget>[
                ElevatedButton(
                  onPressed: () async {
                    await inviteUser(email, profileType, selectedRoleIds);
                    Navigator.of(context).pop();
                  },
                  child: Text('Пригласить'),
                ),
              ],
            );
          } else {
            return Center(child: Text('Нет данных'));
          }
        },
      );
    },
  );
}
