import 'dart:io';
import 'package:flutter/material.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/gestures.dart';
import 'package:video_player/video_player.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
      ),
      home: LessonCreatePage(),
    );
  }
}

class LessonCreatePage extends StatefulWidget {
  @override
  _LessonCreatePageState createState() => _LessonCreatePageState();
}

class _LessonCreatePageState extends State<LessonCreatePage> {
  List<String> mediaItems = [];
  List<TextBlock> textBlocks = [];
  List<AudioBlock> audioBlocks = [];
  List<OpenEndedQuestion> openEndedQuestions = [];
  List<MultipleChoiceQuestion> multipleChoiceQuestions = [];

  TextEditingController textController = TextEditingController();
  bool isHovered = false;

  final LongPressGestureRecognizer longPressGestureRecognizer = LongPressGestureRecognizer();
  TextBlock? selectedTextBlock;

  VideoPlayerController? videoController;

  @override
  void dispose() {
    longPressGestureRecognizer.dispose();
    videoController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple,
        title: Text('          Создать урок'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ...textBlocks.map((textBlock) => Dismissible(
                key: Key('text_${textBlocks.indexOf(textBlock)}'),
                confirmDismiss: (direction) async {
                  return await _confirmDelete();
                },
                onDismissed: (direction) {
                  setState(() {
                    textBlocks.remove(textBlock);
                  });
                },
                background: Container(
                  color: Colors.white12,
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.only(left: 16.0),
                  child: Icon(
                    Icons.delete,
                    color: Colors.white,
                  ),
                ),
                child: GestureDetector(
                  onLongPress: () {
                    _editTextBlock(textBlock);
                  },
                  child: Container(
                    width: double.infinity,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        InkWell(
                          onTap: () {
                            _editTextBlock(textBlock);
                          },
                          onHover: (hovered) {
                            setState(() {
                              isHovered = hovered;
                            });
                          },
                          child: RichTextWidget(textBlock),
                        ),
                        if (isHovered)
                          IconButton(
                            icon: Icon(Icons.edit),
                            onPressed: () {
                              _editTextBlock(textBlock);
                            },
                          ),
                      ],
                    ),
                  ),
                ),
              )),
              ...audioBlocks.map((audioBlock) => Dismissible(
                key: Key('audio_${audioBlocks.indexOf(audioBlock)}'),
                confirmDismiss: (direction) async {
                  return await _confirmDelete();
                },
                onDismissed: (direction) {
                  setState(() {
                    audioBlocks.remove(audioBlock);
                  });
                },
                background: Container(
                  color: Colors.white12,
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.only(left: 16.0),
                  child: Icon(
                    Icons.delete,
                    color: Colors.white,
                  ),
                ),
                child: Container(
                  width: double.infinity,
                  child: AudioBlockWidget(audioBlock),
                ),
              )),
              ...mediaItems.map((filePath) => Dismissible(
                key: Key('media_${mediaItems.indexOf(filePath)}'),
                confirmDismiss: (direction) async {
                  return await _confirmDelete();
                },
                onDismissed: (direction) {
                  setState(() {
                    mediaItems.remove(filePath);
                  });
                },
                background: Container(
                  color: Colors.white12,
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.only(left: 16.0),
                  child: Icon(
                    Icons.delete,
                    color: Colors.white,
                  ),
                ),
                child: getFileWidget(filePath),
              )),
              ...openEndedQuestions.map((question) => OpenEndedQuestionWidget(question)),
              ...multipleChoiceQuestions.map((question) => MultipleChoiceQuestionWidget(question)),
              if (textController.text.isNotEmpty)
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 16.0),
                    Text(
                      textController.text,
                      style: TextStyle(fontSize: 16.0),
                      textAlign: TextAlign.left,
                    ),
                    SizedBox(height: 16.0),
                  ],
                ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _showAddContentDialog();
        },
        child: Icon(Icons.add),

      ),
    );
  }

  Widget getFileWidget(String filePath) {
    if (filePath.toLowerCase().endsWith('.txt')) {
      return Container(
        width: double.infinity,
        child: Text(
          File(filePath).readAsStringSync(),
          style: TextStyle(fontSize: 16.0),
          textAlign: TextAlign.left,
        ),
      );
    } else if (['.jpg', '.jpeg', '.png', '.gif'].any(filePath.toLowerCase().endsWith)) {
      return Container(
        width: double.infinity,
        child: Image.file(
          File(filePath),
          fit: BoxFit.cover,
        ),
      );
    } else if (['.mp4', '.mkv', '.avi'].any(filePath.toLowerCase().endsWith)) {
      videoController = VideoPlayerController.file(File(filePath))
        ..initialize().then((_) {
          videoController!.play();
        });

      return Container(
        width: double.infinity,
        child: AspectRatio(
          aspectRatio: videoController!.value.aspectRatio,
          child: VideoPlayer(videoController!),
        ),
      );
    } else {
      return Container(
        width: double.infinity,
        child: Text(
          'Неизвестный тип файла: ${File(filePath).path.split('/').last}',
          style: TextStyle(fontSize: 16.0),
        ),
      );
    }
  }

  void _addText(String text) {
    setState(() {
      textBlocks.add(TextBlock(text: text));
    });
  }

  void _addMedia(List<String> filePaths) {
    setState(() {
      mediaItems.addAll(filePaths);
    });
  }

  void _addOpenEndedQuestion(String question) {
    setState(() {
      openEndedQuestions.add(OpenEndedQuestion(question: question));
    });
  }

  void _addMultipleChoiceQuestion(
      String question, List<String> options, int correctOptionIndex) {
    setState(() {
      multipleChoiceQuestions.add(
        MultipleChoiceQuestion(
          question: question,
          options: options,
          correctOptionIndex: correctOptionIndex,
        ),
      );
    });
  }

  Future<void> _showAddContentDialog() async {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Добавить контент'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              ElevatedButton(
                onPressed: () async {
                  Navigator.of(context).pop();
                  List<String>? result = await _showFilePicker();
                  if (result != null) {
                    _addMedia(result);
                  }
                },
                child: Text('Загрузить медиа'),
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  _showAddTextDialog();
                },
                child: Text('Добавить текст'),
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  _showAddOpenEndedQuestionDialog();
                },
                child: Text('Добавить открытый вопрос'),
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  _showAddMultipleChoiceQuestionDialog();
                },
                child: Text('Добавить вопрос с выбором'),
              ),
            ],
          ),
        );
      },
    );
  }

  Future<void> _showAddTextDialog() async {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Добавить текст'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextField(
                controller: textController,
                maxLines: null,
                decoration: InputDecoration(
                  hintText: 'Введите текст',
                ),
              ),
              SizedBox(height: 8.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    onPressed: () {
                      setState(() {
                        textController.clear();
                        Navigator.of(context).pop();
                      });
                    },
                    child: Text('Отмена'),
                  ),
                  SizedBox(width: 16.0),
                  ElevatedButton(
                    onPressed: () {
                      setState(() {
                        _addText(textController.text);
                        textController.clear();
                        Navigator.of(context).pop();
                      });
                    },
                    child: Text('Добавить'),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }

  Future<void> _showAddOpenEndedQuestionDialog() async {
    TextEditingController questionController = TextEditingController();
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Добавить открытый вопрос'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextField(
                controller: questionController,
                decoration: InputDecoration(
                  hintText: 'Введите вопрос',
                ),
              ),
              SizedBox(height: 8.0),
              ElevatedButton(
                onPressed: () {
                  setState(() {
                    _addOpenEndedQuestion(questionController.text);
                    questionController.clear();
                    Navigator.of(context).pop();
                  });
                },
                child: Text('Добавить'),
              ),
            ],
          ),
        );
      },
    );
  }

  Future<void> _showAddMultipleChoiceQuestionDialog() async {
    TextEditingController questionController = TextEditingController();
    TextEditingController optionsController = TextEditingController();
    TextEditingController correctOptionController = TextEditingController();

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Добавить вопрос с выбором'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextField(
                controller: questionController,
                decoration: InputDecoration(
                  hintText: 'Введите вопрос',
                ),
              ),
              SizedBox(height: 8.0),
              TextField(
                controller: optionsController,
                decoration: InputDecoration(
                  hintText: 'Введите варианты ответа (через запятую)',
                ),
              ),
              SizedBox(height: 8.0),
              TextField(
                controller: correctOptionController,
                decoration: InputDecoration(
                  hintText: 'Введите номер правильного варианта',
                ),
              ),
              SizedBox(height: 8.0),
              ElevatedButton(
                onPressed: () {
                  List<String> options =
                  optionsController.text.split(',').map((e) => e.trim()).toList();
                  int correctOptionIndex = int.tryParse(correctOptionController.text) ?? 0;

                  setState(() {
                    _addMultipleChoiceQuestion(questionController.text, options, correctOptionIndex);
                    questionController.clear();
                    optionsController.clear();
                    correctOptionController.clear();
                    Navigator.of(context).pop();
                  });
                },
                child: Text('Добавить'),
              ),
            ],
          ),
        );
      },
    );
  }

  Future<List<String>?> _showFilePicker() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(allowMultiple: true);

    if (result != null) {
      return result.paths?.map((file) => file!).toList();
    }
    return null;
  }

  void _editTextBlock(TextBlock textBlock) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Редактирование текста'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextField(
                controller: TextEditingController(text: textBlock.text),
                onChanged: (text) {
                  textBlock.text = text;
                },
                maxLines: null,
                decoration: InputDecoration(
                  hintText: 'Текст',
                ),
              ),
              SizedBox(height: 8.0),
              ElevatedButton(
                onPressed: () {
                  setState(() {
                    Navigator.of(context).pop();
                  });
                },
                child: Text('Сохранить'),
              ),
            ],
          ),
        );
      },
    );
  }

  Future<bool> _confirmDelete() async {
    return await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Подтвердите удаление'),
          content: Text('Вы уверены, что хотите удалить этот элемент?'),
          actions: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pop(false); // Не удаляем
                  },
                  child: Text('Отмена'),
                ),
                SizedBox(width: 16.0),
                ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pop(true); // Удаляем
                  },
                  child: Text('Удалить'),
                ),
              ],
            ),
          ],
        );
      },
    );
  }
}

class TextBlock {
  String text;

  TextBlock({
    required this.text,
  });
}

class RichTextWidget extends StatelessWidget {
  final TextBlock textBlock;

  RichTextWidget(this.textBlock);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Text(
        textBlock.text,
        style: TextStyle(fontSize: 16.0),
        textAlign: TextAlign.left,
      ),
    );
  }
}

class AudioBlock {
  String audioPath;

  AudioBlock({
    required this.audioPath,
  });
}

class AudioBlockWidget extends StatelessWidget {
  final AudioBlock audioBlock;

  AudioBlockWidget(this.audioBlock);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Text(
        'Аудио файл: ${audioBlock.audioPath.split('/').last}',
        style: TextStyle(fontSize: 16.0),
      ),
    );
  }
}

class OpenEndedQuestion {
  String question;
  String answer = ""; // Added answer field

  OpenEndedQuestion({
    required this.question,
  });
}

// ... (previous code)

class OpenEndedQuestionWidget extends StatefulWidget {
  final OpenEndedQuestion question;

  OpenEndedQuestionWidget(this.question);

  @override
  _OpenEndedQuestionWidgetState createState() => _OpenEndedQuestionWidgetState();
}

class _OpenEndedQuestionWidgetState extends State<OpenEndedQuestionWidget> {
  TextEditingController answerController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Открытый вопрос: ${widget.question.question}',
            style: TextStyle(fontSize: 16.0),
          ),
          SizedBox(height: 8.0),
          TextField(
            controller: answerController,
            decoration: InputDecoration(
              hintText: 'Введите ответ',
            ),
          ),
          SizedBox(height: 8.0),
          ElevatedButton(
            onPressed: () {
              setState(() {
                widget.question.answer = answerController.text;
                answerController.clear();
              });
            },
            child: Text('Отправить'),
          ),
        ],
      ),
    );
  }
}

class MultipleChoiceQuestion {
  String question;
  List<String> options;
  int correctOptionIndex;

  MultipleChoiceQuestion({
    required this.question,
    required this.options,
    required this.correctOptionIndex,
  });
}

class MultipleChoiceQuestionWidget extends StatefulWidget {
  final MultipleChoiceQuestion question;

  MultipleChoiceQuestionWidget(this.question);

  @override
  _MultipleChoiceQuestionWidgetState createState() =>
      _MultipleChoiceQuestionWidgetState();
}

class _MultipleChoiceQuestionWidgetState
    extends State<MultipleChoiceQuestionWidget> {
  List<bool> selectedOptions = [];

  @override
  void initState() {
    super.initState();
    selectedOptions =
        List.generate(widget.question.options.length, (index) => false);
    selectedOptions[widget.question.correctOptionIndex] = true;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Вопрос с выбором: ${widget.question.question}',
            style: TextStyle(fontSize: 16.0),
          ),
          SizedBox(height: 8.0),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: widget.question.options
                .asMap()
                .entries
                .map((entry) => Row(
              children: [
                Checkbox(
                  value: selectedOptions[entry.key],
                  onChanged: (value) {
                    setState(() {
                      selectedOptions[entry.key] = value!;
                    });
                  },
                ),
                Text('Вариант ${entry.key + 1}: ${entry.value}'),
              ],
            ))
                .toList(),
          ),
        ],
      ),
    );
  }
}