import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:fruk_app/async/service/firebase_api.dart';
import 'package:fruk_app/firebase_options.dart';
import 'package:fruk_app/model/user/UserAuthRequest.dart';
import 'package:fruk_app/model/user/UserResponse.dart';
import 'package:flutter/material.dart';
import 'package:fruk_app/screen/courses.dart';
import 'package:fruk_app/screen/menu.dart';
import '../constants/HeaderConstants.dart';
import '../constants/HostConstants.dart';
import 'reset_password.dart';
import 'registration.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:provider/provider.dart';
import 'package:fruk_app/async/service/notification_reader.dart';
import 'dart:async';
import 'package:flutter_gen/gen_l10n/app_localization.dart';


class LoginModel extends ChangeNotifier {
  bool _isLoggedIn = false;

  bool get isLoggedIn => _isLoggedIn;

  Future<void> login(BuildContext context, email, password) async {
    // Future<void> _login() async {
    AuthenticationData authData =
    AuthenticationData(email: email, password: password);

    final Uri url = Uri.parse('https://apidev.baze.pro/v1/user/auth');

    final dio = Dio();

    final Map<String, String> headers = {
      HeaderConstants.CONTENT_TYPE: HeaderConstants.CONTENT_TYPE_VALUE,
      HeaderConstants.ORIGIN: HostConstants.HOST,
      HeaderConstants.REFERER: HostConstants.HOST
    };

    try {
      final response = await dio.post(
        url.toString(),
        options: Options(
            headers: headers
        ),
        data: jsonEncode(authData.toJson()),
      );

      if (response.statusCode == 200) {
        Map<String, dynamic> parsedJson = response.data;

        TokenData tokenData = TokenData.fromJson(parsedJson);
        ProfileData profileData = ProfileData.fromJson(parsedJson['profile']);

        // Сохранение данных в SharedPreferences
        final SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString('token', tokenData.token);
        prefs.setString('profile_id', profileData.id.toString());
        prefs.setString('profile_type', profileData.profileType);
        // Сохраните остальные данные профиля по мере необходимости

        print("Успешная авторизация!");
        print(profileData.toString());
        print(tokenData.toString());

        _isLoggedIn = true;
        notifyListeners();
        initScheduledNotificationJob();

        // init firebase
        await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
        await FirebaseApi().initInvocation();
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => CourseListPage()), // Замените ВашНовыйЭкран на фактический экран, на который вы хотите перейти
        );
      } else {
        // Обработка ошибки
        unsecsess(context);
        print('Ошибка входа: ${response.statusCode}');
      }
    } catch (error) {
      unsecsess(context);
      print('Ошибка: $error');
    }
  }
}

void unsecsess (BuildContext context) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(AppLocalizations.of(context)!.login_error),
        content: Text(
            AppLocalizations.of(context)!.something_went_wrong),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
              // Вернуться на экран логина через 5 секунд
              Timer(const Duration(seconds: 5), () {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) =>
                      LoginScreen()), // Замените LoginPage на ваш экран логина
                );
              });
            },
            child: Text(AppLocalizations.of(context)!.ok),
          ),
        ],
      );
    },
  );
}



class LoginScreen extends StatelessWidget{
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  LoginScreen({super.key});

  bool _isLoggedIn = false;

  bool get isLoggedIn => _isLoggedIn;

  @override
  Widget build(BuildContext context) {
    final loginModel = Provider.of<LoginModel>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.login),
      ),
      drawer: loginModel.isLoggedIn?MenuScreen():null,
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            TextField(
              controller: emailController,
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                labelText: AppLocalizations.of(context)!.email,
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(height: 12.0),
            TextField(
              controller: passwordController,
              obscureText: true,
              decoration: InputDecoration(
                labelText: AppLocalizations.of(context)!.password,
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(height: 20.0),
            ElevatedButton(
              onPressed: () async {
                // Simulating an asynchronous login action
                await loginModel.login(context, emailController.text.trim(),passwordController.text.trim() );
              },
              child: Text(AppLocalizations.of(context)!.login),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,


                  MaterialPageRoute(builder: (context) => RegistrationScreen()),
                );
              },
              child: Text(AppLocalizations.of(context)!.no_account),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => InitPasswordResetScreen()),
                );
              },
              child: Text(AppLocalizations.of(context)!.reset_password),
            ),
          ],
        ),
      ),
    );
  }
}
