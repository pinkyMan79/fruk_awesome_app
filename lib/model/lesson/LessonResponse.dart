class LessonResponse {
  int id;
  String name;
  String? iconUrl;
  String? headerUrl;
  bool access;
  String status;
  String? openCondition;
  DateTime? openAt;
  int? openAfterId;
  int? openAwaiting;
  bool? isOpen;

  LessonResponse({
    required this.id,
    required this.name,
    this.iconUrl,
    this.headerUrl,
    required this.access,
    required this.status,
    this.openCondition,
    this.openAt,
    this.openAfterId,
    this.openAwaiting,
    this.isOpen,
  });

  factory LessonResponse.fromJson(Map<String, dynamic> json) {
    return LessonResponse(
      id: json['id'] as int,
      name: json['name'] as String,
      iconUrl: json['iconUrl']!=null? json['iconUrl'] as String:null,
      headerUrl: json['headerUrl']!=null? json['headerUrl'] as String:null,
      access: json['access'] as bool,
      status: json['status'] as String,
      openCondition: json['openCondition'] != null
          ? json['openCondition'] as String
          : null,
      openAt: json['openAt'] != null
          ? DateTime.parse(json['openAt'] as String)
          : DateTime.now(),
      openAfterId:
          json['openAfterId'] != null ? json['openAfterId'] as int : null,
      openAwaiting:
          json['openAwaiting'] != null ? json['openAwaiting'] as int : null,
      isOpen: json['isOpen'] != null ? json['isOpen'] as bool : null,
    );
  }

  @override
  String toString() {
    return 'LessonResponse{id: $id, name: $name, iconUrl: $iconUrl, headerUrl: $headerUrl, access: $access, status: $status}';
  }
}
