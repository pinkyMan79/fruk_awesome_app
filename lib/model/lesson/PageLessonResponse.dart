import 'package:fruk_app/model/course/response/PageableObject.dart';
import 'package:fruk_app/model/course/response/SortObject.dart';

import 'LessonResponse.dart';

class PageLessonResponse {
  int totalPages;
  int totalElements;
  String? pageable;
  int size;
  List<LessonResponse> content;
  int number;
  SortObject sort;
  bool first;
  bool last;
  int numberOfElements;
  bool empty;

  PageLessonResponse({
    required this.totalPages,
    required this.totalElements,
    this.pageable,
    required this.size,
    required this.content,
    required this.number,
    required this.sort,
    required this.first,
    required this.last,
    required this.numberOfElements,
    required this.empty,
  });

  factory PageLessonResponse.fromJson(Map<String, dynamic> json) {
    return PageLessonResponse(
      totalPages: json['totalPages'] as int,
      totalElements: json['totalElements'] as int,

      size: json['size'] as int,
      content: (json['content'] as List<dynamic>)
          .map((item) => LessonResponse.fromJson(item as Map<String, dynamic>))
          .toList(),
      number: json['number'] as int,

      first: json['first'] as bool,
      last: json['last'] as bool,
      numberOfElements: json['numberOfElements'] as int,
      empty: json['empty'] as bool,
      pageable: json['pageable']!=null? json['pageable'] as String:null,
      sort: SortObject.fromJson(json['sort'] as Map<String, dynamic>),
    );
  }
}