class LessonTemplateResponse {
  final int uuid;
  final String type;
  final String data;
  final String? buttonName;

  LessonTemplateResponse({
    required this.uuid,
    required this.type,
    required this.data,
    this.buttonName,
  });

  factory LessonTemplateResponse.fromJson(Map<String, dynamic> json) {
    return LessonTemplateResponse(
      uuid: json['uuid'],
      type: json['type'],
      data: json['data'],
      buttonName: json['buttonName']!=null ? json['buttonName']:null,
    );
  }
}
class LessonsList{
  final List<InnerLessonsList> data;
  LessonsList({required this.data});

  factory LessonsList.fromJson(Map<String, dynamic> json) {
    return LessonsList(
      data: json[''] ?? '',
    );
  }
}

class InnerLessonsList{
  final List<LessonTemplateResponse> data;
  InnerLessonsList({required this.data});

  factory InnerLessonsList.fromJson(Map<String, dynamic> json) {
    return InnerLessonsList(
      data: json[''] ?? '',
    );
  }
}
