class CompleteAnswer {
  int? questionId;
  int? pickedAnswer;
  String? answer;
  bool? error;
  bool? open;

  CompleteAnswer({
    this.questionId,
    this.pickedAnswer,
    this.answer,
    this.error,
    this.open
  });


  Map<String, dynamic> toJson() {
    return {
      'questionId': questionId,
      'pickedAnswer': pickedAnswer,
      'answer': answer,
      'error': error,
      'open': open
    };
  }

  @override
  String toString() {
    return 'CompleteAnswer{questionId: $questionId, pickedAnswer: $pickedAnswer, answer: $answer, error: $error, open: $open}';
  }
}