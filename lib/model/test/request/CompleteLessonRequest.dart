import 'CompleteAnswer.dart';

class CompleteLessonRequest {
    final int? version;
    final List<CompleteAnswer?>? answers;

    CompleteLessonRequest({
        this.version,
        this.answers,
    });

    Map<String, dynamic> toJson() {
        return {
            'version': version,
            'email': answers?.map((item) => item?.toJson()).toList(),
        };
    }

    @override
  String toString() {
    return 'CompleteLessonRequest{version: $version, answers: $answers}';
  }
}

