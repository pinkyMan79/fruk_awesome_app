import 'LessonAnswerResponse.dart';

class TestQuestionResponse {
  final int? id;
  final bool? open;
  final String? question;
  final List<LessonAnswerResponse?>? answers;

  TestQuestionResponse({
    this.id,
    this.open,
    this.question,
    this.answers,
  });


  factory TestQuestionResponse.fromJson(Map<String, dynamic> json) {
    return TestQuestionResponse(
      id: json['id'] as int?,
      open: json['open'] as bool?,
      question: json['question'] as String?,
      answers: (json['answers'] as List<dynamic>)
          .map((item) => LessonAnswerResponse.fromJson(item as Map<String, dynamic>))
          .toList()
    );
  }

}

