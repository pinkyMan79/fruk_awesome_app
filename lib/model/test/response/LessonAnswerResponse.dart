class LessonAnswerResponse {
  final int? id;
  final String? text;
  final bool? right;

  LessonAnswerResponse({
    this.id,
    this.text,
    this.right,
  });


  factory LessonAnswerResponse.fromJson(Map<String, dynamic> json) {
    return LessonAnswerResponse(
        id: json['id'] as int,
        text: json['text'] as String,
        right: json['right'] as bool,
    );
  }
}