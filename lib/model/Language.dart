class Language {
  final int id;
  final String name;
  final String languageCode;

  Language(this.id, this.languageCode, this.name);

  static List<Language> languageList() {
    return <Language>[
        Language(1, "Русский", "ru"),
        Language(2, "English", "en")
    ];
  }
}