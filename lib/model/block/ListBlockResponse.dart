import 'BlockResponse.dart';

class ListBlockResponse {
  final List<BlockResponse> blocks;

  ListBlockResponse({required this.blocks});

  factory ListBlockResponse.fromJson(List<dynamic> json) {
    List<BlockResponse> blockList =
    json.map((block) => BlockResponse.fromJson(block)).toList();
    return ListBlockResponse(blocks: blockList);
  }
}