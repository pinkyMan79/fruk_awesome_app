import 'dart:io';

class BlockRequest {
  late String name;
  late int courseId;
  late File multipartFile;

  BlockRequest({required this.name, required this.courseId, required this.multipartFile});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {
      'name': name,
      'courseId': courseId,
      // You might need to handle 'multipartFile' accordingly based on your API requirements
      // For instance, convert it to bytes, base64, or send it using FormData
      // Example using FormData:
      // 'multipartFile': await MultipartFile.fromFile(multipartFile.path),
    };
    return data;
  }
}