class BlockResponse {
  final int id;
  final String name;
  final String? avatarUrl;
  final String? status;
  final int countLessons;

  BlockResponse({
    required this.id,
    required this.name,
    this.avatarUrl,
    this.status,
    required this.countLessons,
  });

  factory BlockResponse.fromJson(Map<String, dynamic> json) {
    return BlockResponse(
      id: json['id'] as int,
      name: json['name'] as String,
      avatarUrl: json['avatarUrl'] as String?,
      status: json['status'] as String?,
      countLessons: json['countLessons'] as int,
    );
  }
}