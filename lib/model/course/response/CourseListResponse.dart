import 'AttachmentResponse.dart';

class CourseListResponse {
  final int id;
  final String name;
  final String description;
  final AttachmentResponse attachmentsUrls;
  final bool passed;
  final int countBlocks;
  final int countStudents;
  final int countLessons;
  final String createdAt;
  final String? updatedAt;

  CourseListResponse({
    required this.id,
    required this.name,
    required this.description,
    required this.attachmentsUrls,
    required this.passed,
    required this.countBlocks,
    required this.countStudents,
    required this.countLessons,
    required this.createdAt,
    this.updatedAt,
  });

  factory CourseListResponse.fromJson(Map<String, dynamic> json) {
    return CourseListResponse(
      id: json['id'] as int,
      name: json['name'] as String,
      description: json['description'] as String,
      attachmentsUrls: (AttachmentResponse.fromJson(json['attachmentsUrls'] as Map<String, dynamic>) ),
      passed: json['passed'] as bool,
      countBlocks: json['countBlocks'] as int,
      countStudents: json['countStudents'] as int,
      countLessons: json['countLessons'] as int,
      createdAt: json['createdAt'] as String,
      updatedAt: json['updatedAt']!=null ?json['updatedAt'] as String:null,
    );
  }

  @override
  String toString() {
    return 'CourseListResponse{id: $id, name: $name, description: $description, attachmentsUrls: $attachmentsUrls, passed: $passed, countBlocks: $countBlocks, countStudents: $countStudents, countLessons: $countLessons, createdAt: $createdAt, updatedAt: $updatedAt}';
  }
}
