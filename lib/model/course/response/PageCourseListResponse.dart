import 'CourseListResponse.dart';
import 'PageableObject.dart';
import 'SortObject.dart';

class PageCourseListResponse {
    final int totalPages;
    final int totalElements;
    final int size;
    final int number;
    final bool first;
    final bool last;
    final int numberOfElements;
    final bool empty;
    final List<CourseListResponse> content;
    final PageableObject pageable;
    final SortObject sort;

    PageCourseListResponse({
        required this.totalPages,
        required this.totalElements,
        required this.size,
        required this.number,
        required this.first,
        required this.last,
        required this.numberOfElements,
        required this.empty,
        required this.content,
        required this.pageable,
        required this.sort,
    });

    factory PageCourseListResponse.fromJson(Map<String, dynamic> json) {
        return PageCourseListResponse(
            totalPages: json['totalPages'] as int,
            totalElements: json['totalElements'] as int,
            size: json['size'] as int,
            number: json['number'] as int,
            first: json['first'] as bool,
            last: json['last'] as bool,
            numberOfElements: json['numberOfElements'] as int,
            empty: json['empty'] as bool,
            content: (json['content'] as List<dynamic>)
                .map((item) => CourseListResponse.fromJson(item as Map<String, dynamic>))
                .toList(),
            pageable: PageableObject.fromJson(json['pageable'] as Map<String, dynamic>),
            sort: SortObject.fromJson(json['sort'] as Map<String, dynamic>),
        );
    }

    @override
  String toString() {
    return 'PageCourseListResponse{totalPages: $totalPages, totalElements: $totalElements, size: $size, number: $number, first: $first, last: $last, numberOfElements: $numberOfElements, empty: $empty, content: $content, pageable: $pageable, sort: $sort}';
  }
}
