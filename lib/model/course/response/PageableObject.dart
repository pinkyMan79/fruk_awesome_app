import 'SortObject.dart';

class PageableObject {
  final int? pageNumber;
  final int? pageSize;
  final int? offset;
  final SortObject? sort;
  final bool? unpaged;
  final bool? paged;

  PageableObject({
    this.pageNumber,
    this.pageSize,
    this.offset,
    this.sort,
    this.unpaged,
    this.paged,
  });


  factory PageableObject.fromJson(Map<String, dynamic> json) {
    return PageableObject(
      pageNumber: json['pageNumber'] as int,
      pageSize: json['pageSize'] as int,
      offset: json['offset'] as int,
      sort: SortObject.fromJson(json['sort'] as Map<String, dynamic>),
      unpaged: json['unpaged']!=null? json['unpaged'] as bool:null,
      paged: json['paged']!=null? json['paged'] as bool:null,
    );
  }

  @override
  String toString() {
    return 'PageableObject{pageNumber: $pageNumber, pageSize: $pageSize, offset: $offset, sort: $sort, unpaged: $unpaged, paged: $paged}';
  }
}
