class SortObject {
  final bool? sorted;
  final bool? empty;
  final bool? unsorted;

  SortObject({
    this.sorted,
    this.empty,
    this.unsorted,
  });

  factory SortObject.fromJson(Map<String, dynamic> json) {
    return SortObject(
      sorted: json['sorted'] != null ? json['sorted'] as bool : null,
      empty: json['empty'] != null ? json['empty'] as bool : null,
      unsorted: json['unsorted'] != null ? json['unsorted'] as bool : null,
    );
  }

  @override
  String toString() {
    return 'SortObject{sorted: $sorted, empty: $empty, unsorted: $unsorted}';
  }
}
