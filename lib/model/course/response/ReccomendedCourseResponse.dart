import 'AttachmentResponse.dart';


class ReccommendedCoursesResponse {
  final List<String> recommendedCourses;

  ReccommendedCoursesResponse({
    required this.recommendedCourses,
  });

  factory ReccommendedCoursesResponse.fromJson(Map<String, dynamic> json) {
    var coursesList = json['recommended_courses'] as List<dynamic>;

    List<String> courses = coursesList.map((courseJson) {
      return courseJson.toString(); // Предполагается, что в JSON приходят строки
    }).toList();

    return ReccommendedCoursesResponse(recommendedCourses: courses);
  }


}
// class ReccommendedCoursesResponse {
//   final List<String> data;
//   // final String data;
//
//   ReccommendedCoursesResponse({
//     required this.data
//   });
//
//   factory ReccommendedCoursesResponse.fromJson(Map<String, dynamic> json) {
//     return ReccommendedCoursesResponse(
//       // data: json['recommended_courses'] as String
//
//         data: (json['content'] as List<dynamic>)
//         .map((item) => item String))
//         .toList(),
//     );
//   }
// }