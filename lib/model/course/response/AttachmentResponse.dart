class AttachmentResponse {
  final Map<String, dynamic> urls;

  AttachmentResponse({required this.urls});

  factory AttachmentResponse.fromJson(Map<String, dynamic> json) {
    return AttachmentResponse(urls: json);
  }

  @override
  String toString() {
    return 'AttachmentResponse{urls: $urls}';
  }
}