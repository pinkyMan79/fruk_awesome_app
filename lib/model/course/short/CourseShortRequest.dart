class CourseShortRequest {
  final int id;
  final String name;
  final bool passed;
  final String description;

  CourseShortRequest({
    required this.id,
    required this.name,
    required this.passed,
    required this.description,
  });

  factory CourseShortRequest.fromJson(Map<String, dynamic> json) {
    return CourseShortRequest(
      id: json['id'] as int,
      name: json['name'] as String,
      passed: json['passed'] as bool,
      description: json['description'] as String,
    );
  }
}