class Course {
  final int id;
  final String name;
  final bool passed;
  final String description;

  Course({
    required this.id,
    required this.name,
    required this.passed,
    required this.description,
  });

  factory Course.fromJson(Map<String, dynamic> json) {
    return Course(
      id: json['id'] as int,
      name: json['name'] as String,
      passed: json['passed'] as bool,
      description: json['description'] as String,
    );
  }
}