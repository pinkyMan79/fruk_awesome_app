class CourseInitRequestData {
    final String name;
    final String description;

    CourseInitRequestData({
      required this.name,
      required this.description,
    });

    Map<String, dynamic> toJson() {
      return {
        'name': name,
        'description': description,
      };
    }
}