class CourseUpdateRequestData {
  final String name;
  final String description;

  CourseUpdateRequestData({
    required this.name,
    required this.description,
  });

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'description': description,
    };
  }
}