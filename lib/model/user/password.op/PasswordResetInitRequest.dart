class PasswordResetInitData {
  final String email;

  PasswordResetInitData({
    required this.email,
  });

  factory PasswordResetInitData.fromJson(Map<String, dynamic> json) {
    return PasswordResetInitData(
      email: json['email'] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'email': email,
    };
  }
}
