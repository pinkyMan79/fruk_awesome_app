class PasswordResetData {
  final String token;
  final String password;
  final String passwordConfirmation;

  PasswordResetData({
    required this.token,
    required this.password,
    required this.passwordConfirmation,
  });

  factory PasswordResetData.fromJson(Map<String, dynamic> json) {
    return PasswordResetData(
      token: json['token'] ?? '',
      password: json['password'] ?? '',
      passwordConfirmation: json['passwordConfirmation'] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'token': token,
      'password': password,
      'passwordConfirmation': passwordConfirmation,
    };
  }
}
