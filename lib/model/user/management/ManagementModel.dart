class Role {
  final int id;
  final String name;
  final List<Course> courses;

  Role({
    required this.id,
    required this.name,
    required this.courses,
  });

  factory Role.fromJson(Map<String, dynamic> json) {
    return Role(
      id: json['id'],
      name: json['name'],
      courses: (json['courses'] as List<dynamic>)
          .map((courseJson) => Course.fromJson(courseJson))
          .toList(),
    );
  }
}

class Course {
  final int courseId;
  final String name;

  Course({
    required this.courseId,
    required this.name,
  });

  factory Course.fromJson(Map<String, dynamic> json) {
    return Course(
      courseId: json['courseId'],
      name: json['name'],
    );
  }
}
