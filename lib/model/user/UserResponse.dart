class User {
  final int id;
  final String firstName;
  final String lastName;
  final String phone;
  final String profileType;
  final DateTime createdAt;
  final bool active;
  final int banned;
  final String companyName;
  final int companyId;
  final bool needBanner;
  final int reportDate;
  final DateTime paymentPeriod;
  final bool isTestActive;

  User({
    required this.id,
    required this.firstName,
    required this.lastName,
    required this.phone,
    required this.profileType,
    required this.createdAt,
    required this.active,
    required this.banned,
    required this.companyName,
    required this.companyId,
    required this.needBanner,
    required this.reportDate,
    required this.paymentPeriod,
    required this.isTestActive,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      id: json['id'] ?? 0,
      firstName: json['firstName'] ?? '',
      lastName: json['lastName'] ?? '',
      phone: json['phone'] ?? '',
      profileType: json['profileType'] ?? '',
      createdAt: DateTime.parse(json['createdAt'] ?? ''),
      active: json['active'] ?? false,
      banned: json['banned'] ?? 0,
      companyName: json['companyName'] ?? '',
      companyId: json['companyId'] ?? 0,
      needBanner: json['needBanner'] ?? false,
      reportDate: json['reportDate'] ?? 0,
      paymentPeriod: DateTime.parse(json['paymentPeriod'] ?? ''),
      isTestActive: json['isTestActive'] ?? false,
    );
  }
}

class TokenData {
  final String token;

  TokenData({required this.token});

  factory TokenData.fromJson(Map<String, dynamic> json) {
    return TokenData(
      token: json['token'] ?? '',
    );
  }
}

class ProfileData {
  final int id;
  final String? firstName;
  final String? lastName;
  final String? phone;
  final String profileType;
  final DateTime createdAt;
  final bool active;
  final int banned;
  final String companyName;
  final int companyId;
  final bool needBanner;
  final int reportDate;
  final DateTime paymentPeriod;
  final bool isTestActive;

  ProfileData({
    required this.id,
    this.firstName,
    this.lastName,
    this.phone,
    required this.profileType,
    required this.createdAt,
    required this.active,
    required this.banned,
    required this.companyName,
    required this.companyId,
    required this.needBanner,
    required this.reportDate,
    required this.paymentPeriod,
    required this.isTestActive,
  });

  factory ProfileData.fromJson(Map<String, dynamic> json) {
    return ProfileData(
      id: json['id'] ?? 0,
      firstName: json['firstName'],
      lastName: json['lastName'],
      phone: json['phone'],
      profileType: json['profileType'] ?? '',
      createdAt: DateTime.parse(json['createdAt'] ?? ''),
      active: json['active'] ?? false,
      banned: json['banned'] ?? 0,
      companyName: json['companyName'] ?? '',
      companyId: json['companyId'] ?? 0,
      needBanner: json['needBanner'] ?? false,
      reportDate: json['reportDate'] ?? 0,
      paymentPeriod: DateTime.parse(json['paymentPeriod'] ?? ''),
      isTestActive: json['isTestActive'] ?? false,
    );
  }
}

